# Vinland Solutions CommonCore.Badging Library
> A Common Core for Svg Badge Generation

[![License](https://commoncorelibs.gitlab.io/commoncore-badging/badge/license.svg)](https://commoncorelibs.gitlab.io/commoncore-badging/license.html)
[![Platform](https://commoncorelibs.gitlab.io/commoncore-badging/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Gitlab Repository](https://commoncorelibs.gitlab.io/commoncore-badging/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-badging)
[![Gitlab Releases](https://commoncorelibs.gitlab.io/commoncore-badging/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-badging/-/releases)
[![Gitlab Documentation](https://commoncorelibs.gitlab.io/commoncore-badging/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-badging/)  
[![Nuget Package](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.Badging/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Badging/)
[![Pipeline Status](https://gitlab.com/commoncorelibs/commoncore-badging/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-badging/pipelines)
[![Coverage Status](https://gitlab.com/commoncorelibs/commoncore-badging/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-badging/reports/index.html)
[![Tests Status](https://commoncorelibs.gitlab.io/commoncore-badging/badge/tests.svg)](https://commoncorelibs.gitlab.io/commoncore-badging/reports/index.html)

CommonCore.Badging is a .Net Standard library for creating svg repo badges such as those commonly found on code repository sites such as GitLab and GitHub.

The library provides an abstraction of a badge as a collection of sections, each section being composed of a color for the background and one or both an icon image and a string of text. A section also allows control over various aspects of how these components are displayed, such padding and font. In this way, a wide variety of badges can be generated.

## Installation

The official release versions of the Badging Badger are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Badging/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

## Usage

All functionality resides within the `CommonCore.Badging` namespace and its entry point is the `Badge` class to which `BadgeSection` objects can be added. The `Badge` provides constructors that allow quick creation of a wide range of common badge compositions. Additionally, the `Badge` includes fluent-style `Add()` methods that greatly simplify the building of multi-section badges.

Each `BadgeSection` is composed of two primary datums: icon and text. Both are optional, but if neither is present then the section will be ignored by any rendering. The `BadgeSection` also provides a style object for both pieces of content which can control the background color, foreground color, padding, etc; though the former is the most important as it controls what color the badge will be behind the icon or text and appropriate color-based overloads make changing it easy.

Section text is straight forward, described adequately by its name and type, is string; however, icon requires more explanation. The library allows the embedding of arbitrary svg images within badges by encoding them as base64. The `BadgeIcons` class exposes a large number of pre-encoded svg images that can be used as icons.

Once the desired sections are added to the badge, it can be output as raw svg or as base64 encoded svg, to be saved or copied to where ever it is needed.

### Example 1
![Badge Example 1](./images/badge_some_repo_name.svg)
```csharp
var badge = new Badge(BadgeIcons.Gitlab, "some repo name", BadgeColors.DarkRed);
Console.WriteLine(badge.ToBase64());
Console.WriteLine(badge.ToSvg());
File.WriteAllText("somefile.svg", badge.ToSvg(), Encoding.UTF8);
```

### Example 2
![Badge Example 2](./images/badge_information.svg)
```csharp
var badge = new Badge(BadgeIcons.Info).Add("information", BadgeColors.BBlue);
Console.WriteLine(badge.ToBase64());
Console.WriteLine(badge.ToSvg());
File.WriteAllText("somefile.svg", badge.ToSvg(), Encoding.UTF8);
```

## Projects

The CommonCore.Badging repository is composed of three projects with the listed dependencies:

* CommonCore.Badging: The dotnet standard library project.
  * [System.Drawing.Common](https://www.nuget.org/packages/System.Drawing.Common/)
* CommonCore.Badging.Docs: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* CommonCore.Badging.Tests: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-badging
- Issues: https://gitlab.com/commoncorelibs/commoncore-badging/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-badging/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Badging/

* [BadgingBadger](https://gitlab.com/vinlandsolutions/badgingbadger): Official console and editor apps for generating badges with this library.

## Alternatives

This is a software library for coders, not an application for average users. If you want to generate a badge online, then try one of the third-party sites below:

* [Badgen.net](https://badgen.net/)
* [Shields.io](https://shields.io/)

For a python library that inspired this one (and is used by this repo's CICD):

* [AnyBadge](https://github.com/jongracecox/anybadge)

## Credits

These two libraries are used by the repo's CICD pipeline and are not a part of the library itself.

* [AnyBadge](https://github.com/jongracecox/anybadge)
* [semver](https://github.com/k-bx/python-semver)
