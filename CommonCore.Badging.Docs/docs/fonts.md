﻿# BadgeFonts

The `BadgeFonts` static class contains a currently limited number of fonts as member properties. These fonts are considered fairly cross-platform compatible and the library may give special consideration when building badges with them. Additional fonts could be added and accessed through the `Dict` dictionary property.

## Font List

Count: 10

| Index |                Title | Example                                                                              |
|------:|---------------------:|:-------------------------------------------------------------------------------------|
|     0 |                      | ![empty Font Badge](../images/infos/fonts/empty.svg)                                 |
|     1 |              Default | ![Default Font Badge](../images/infos/fonts/Default.svg)                             |
|     2 |                Arial | ![Arial Font Badge](../images/infos/fonts/Arial.svg)                                 |
|     3 |           ArialBlack | ![ArialBlack Font Badge](../images/infos/fonts/ArialBlack.svg)                       |
|     4 |           CourierNew | ![CourierNew Font Badge](../images/infos/fonts/CourierNew.svg)                       |
|     5 |           DejaVuSans | ![DejaVuSans Font Badge](../images/infos/fonts/DejaVuSans.svg)                       |
|     6 |              Georgia | ![Georgia Font Badge](../images/infos/fonts/Georgia.svg)                             |
|     7 |               Impact | ![Impact Font Badge](../images/infos/fonts/Impact.svg)                               |
|     8 |        TimesNewRoman | ![TimesNewRoman Font Badge](../images/infos/fonts/TimesNewRoman.svg)                 |
|     9 |              Verdana | ![Verdana Font Badge](../images/infos/fonts/Verdana.svg)                             |
