﻿# Badge Examples

Here are some basic examples of badges created by the Badging Badger. For examples of specific commands, please the other pages in this section.

| Examples |
|---------|
| ![Badge Standard](../images/badge_standard.svg) |
| `new Badge(BadgeIcons.Info).Add("Some Information", BadgeColors.Blue)` |
| ![Badge Simple](../images/badge_simple.svg) |
| `new Badge(BadgeIcons.Info, "label text", BadgeColors.Orange)` |
| ![Badge Standard](../images/badge_multisection.svg) |
| `new Badge(BadgeIcons.Info).Add("Some Information", BadgeColors.Blue).Add("value:with:colons", BadgeColors.Red))` |
| ![Badge Unit Tests](../images/badge_tests_and_coverage.svg) |
| `new Badge("tests").Add("✔ 100 | ✘ 0", BadgeColors.Green).Add("74.28%", BadgeColors.Yellow))` |
