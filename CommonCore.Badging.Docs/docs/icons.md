﻿# BadgeIcons

The `BadgeIcons` static class contains a large number of icons as member properties. Additional icons can be added and accessed through the `Dict` dictionary property.

Before adding a svg image as a `BadgeIcons` icon, the following steps must be taken:

- If needed, the image should be [resized](https://www.iloveimg.com/resize-image/resize-svg) to 24x24 with `viewBox="0 0 24 24"` added to the svg element.
- The image must be [optimized](https://jakearchibald.github.io/svgomg/) to avoid wasted space.
- Convert the svg image to a base64 encoded string.

## Icon List

Count: 98 (140,092 bytes)

| Index |                Title | Size (Bytes) | Example                                                                              |
|------:|---------------------:|-------------:|:-------------------------------------------------------------------------------------|
|     0 |               airbnb |        1,008 | ![airbnb Icon Badge](../images/infos/icons/airbnb.svg)                               |
|     1 |               amazon |        2,856 | ![amazon Icon Badge](../images/infos/icons/amazon.svg)                               |
|     2 |            amazonaws |        4,176 | ![amazonaws Icon Badge](../images/infos/icons/amazonaws.svg)                         |
|     3 |              android |        1,412 | ![android Icon Badge](../images/infos/icons/android.svg)                             |
|     4 |               apache |        4,500 | ![apache Icon Badge](../images/infos/icons/apache.svg)                               |
|     5 |                apple |          692 | ![apple Icon Badge](../images/infos/icons/apple.svg)                                 |
|     6 |             appveyor |          600 | ![appveyor Icon Badge](../images/infos/icons/appveyor.svg)                           |
|     7 |                 atom |        2,236 | ![atom Icon Badge](../images/infos/icons/atom.svg)                                   |
|     8 |              audible |          832 | ![audible Icon Badge](../images/infos/icons/audible.svg)                             |
|     9 |              awesome |          400 | ![awesome Icon Badge](../images/infos/icons/awesome.svg)                             |
|    10 |              bitcoin |        1,188 | ![bitcoin Icon Badge](../images/infos/icons/bitcoin.svg)                             |
|    11 |              blender |        1,704 | ![blender Icon Badge](../images/infos/icons/blender.svg)                             |
|    12 |         buymeacoffee |          536 | ![buymeacoffee Icon Badge](../images/infos/icons/buymeacoffee.svg)                   |
|    13 |             circleci |          720 | ![circleci Icon Badge](../images/infos/icons/circleci.svg)                           |
|    14 |             codebeat |          616 | ![codebeat Icon Badge](../images/infos/icons/codebeat.svg)                           |
|    15 |          codeclimate |          312 | ![codeclimate Icon Badge](../images/infos/icons/codeclimate.svg)                     |
|    16 |              codecov |        1,496 | ![codecov Icon Badge](../images/infos/icons/codecov.svg)                             |
|    17 |             codeship |        1,660 | ![codeship Icon Badge](../images/infos/icons/codeship.svg)                           |
|    18 |             commonwl |          548 | ![commonwl Icon Badge](../images/infos/icons/commonwl.svg)                           |
|    19 |      creativecommons |        1,552 | ![creativecommons Icon Badge](../images/infos/icons/creativecommons.svg)             |
|    20 |                 css3 |          392 | ![css3 Icon Badge](../images/infos/icons/css3.svg)                                   |
|    21 |             deepscan |        1,268 | ![deepscan Icon Badge](../images/infos/icons/deepscan.svg)                           |
|    22 |           dependabot |        1,308 | ![dependabot Icon Badge](../images/infos/icons/dependabot.svg)                       |
|    23 |              discord |        2,384 | ![discord Icon Badge](../images/infos/icons/discord.svg)                             |
|    24 |               disqus |          636 | ![disqus Icon Badge](../images/infos/icons/disqus.svg)                               |
|    25 |              dockbit |        1,288 | ![dockbit Icon Badge](../images/infos/icons/dockbit.svg)                             |
|    26 |               docker |        1,140 | ![docker Icon Badge](../images/infos/icons/docker.svg)                               |
|    27 |               dotnet |          956 | ![dotnet Icon Badge](../images/infos/icons/dotnet.svg)                               |
|    28 |              dropbox |          420 | ![dropbox Icon Badge](../images/infos/icons/dropbox.svg)                             |
|    29 |                 ebay |        1,676 | ![ebay Icon Badge](../images/infos/icons/ebay.svg)                                   |
|    30 |              eclipse |        1,016 | ![eclipse Icon Badge](../images/infos/icons/eclipse.svg)                             |
|    31 |             facebook |          628 | ![facebook Icon Badge](../images/infos/icons/facebook.svg)                           |
|    32 |            filezilla |        3,320 | ![filezilla Icon Badge](../images/infos/icons/filezilla.svg)                         |
|    33 |                 flow |        1,032 | ![flow Icon Badge](../images/infos/icons/flow.svg)                                   |
|    34 |                  git |          932 | ![git Icon Badge](../images/infos/icons/git.svg)                                     |
|    35 |               github |          908 | ![github Icon Badge](../images/infos/icons/github.svg)                               |
|    36 |               gitlab |          372 | ![gitlab Icon Badge](../images/infos/icons/gitlab.svg)                               |
|    37 |               gitpod |          420 | ![gitpod Icon Badge](../images/infos/icons/gitpod.svg)                               |
|    38 |               gitter |          400 | ![gitter Icon Badge](../images/infos/icons/gitter.svg)                               |
|    39 |                gmail |          404 | ![gmail Icon Badge](../images/infos/icons/gmail.svg)                                 |
|    40 |               google |          520 | ![google Icon Badge](../images/infos/icons/google.svg)                               |
|    41 |         googlechrome |          864 | ![googlechrome Icon Badge](../images/infos/icons/googlechrome.svg)                   |
|    42 |           googleplay |          632 | ![googleplay Icon Badge](../images/infos/icons/googleplay.svg)                       |
|    43 |              graphql |          920 | ![graphql Icon Badge](../images/infos/icons/graphql.svg)                             |
|    44 |             gravatar |          568 | ![gravatar Icon Badge](../images/infos/icons/gravatar.svg)                           |
|    45 |              haskell |          336 | ![haskell Icon Badge](../images/infos/icons/haskell.svg)                             |
|    46 |                html5 |          420 | ![html5 Icon Badge](../images/infos/icons/html5.svg)                                 |
|    47 |                 imdb |        1,812 | ![imdb Icon Badge](../images/infos/icons/imdb.svg)                                   |
|    48 |                 info |        1,240 | ![info Icon Badge](../images/infos/icons/info.svg)                                   |
|    49 |             inkscape |        1,416 | ![inkscape Icon Badge](../images/infos/icons/inkscape.svg)                           |
|    50 |            instagram |        2,388 | ![instagram Icon Badge](../images/infos/icons/instagram.svg)                         |
|    51 |     internetexplorer |        1,296 | ![internetexplorer Icon Badge](../images/infos/icons/internetexplorer.svg)           |
|    52 |                 java |        1,684 | ![java Icon Badge](../images/infos/icons/java.svg)                                   |
|    53 |           javascript |        1,272 | ![javascript Icon Badge](../images/infos/icons/javascript.svg)                       |
|    54 |               jquery |        4,540 | ![jquery Icon Badge](../images/infos/icons/jquery.svg)                               |
|    55 |                 json |        3,440 | ![json Icon Badge](../images/infos/icons/json.svg)                                   |
|    56 |                 kofi |          712 | ![kofi Icon Badge](../images/infos/icons/kofi.svg)                                   |
|    57 |           kubernetes |        4,728 | ![kubernetes Icon Badge](../images/infos/icons/kubernetes.svg)                       |
|    58 |                 lgtm |        4,812 | ![lgtm Icon Badge](../images/infos/icons/lgtm.svg)                                   |
|    59 |            libraries |        1,008 | ![libraries Icon Badge](../images/infos/icons/libraries.svg)                         |
|    60 |                linux |        7,572 | ![linux Icon Badge](../images/infos/icons/linux.svg)                                 |
|    61 |      linuxfoundation |          224 | ![linuxfoundation Icon Badge](../images/infos/icons/linuxfoundation.svg)             |
|    62 |             markdown |          528 | ![markdown Icon Badge](../images/infos/icons/markdown.svg)                           |
|    63 |                 mega |          732 | ![mega Icon Badge](../images/infos/icons/mega.svg)                                   |
|    64 |              mozilla |        1,712 | ![mozilla Icon Badge](../images/infos/icons/mozilla.svg)                             |
|    65 |       mozillafirefox |        2,832 | ![mozillafirefox Icon Badge](../images/infos/icons/mozillafirefox.svg)               |
|    66 |                  now |          232 | ![now Icon Badge](../images/infos/icons/now.svg)                                     |
|    67 |                  npm |        1,708 | ![npm Icon Badge](../images/infos/icons/npm.svg)                                     |
|    68 |                nuget |        1,044 | ![nuget Icon Badge](../images/infos/icons/nuget.svg)                                 |
|    69 |              palette |          720 | ![palette Icon Badge](../images/infos/icons/palette.svg)                             |
|    70 |              patreon |          392 | ![patreon Icon Badge](../images/infos/icons/patreon.svg)                             |
|    71 |               paypal |        1,504 | ![paypal Icon Badge](../images/infos/icons/paypal.svg)                               |
|    72 |                  php |        1,484 | ![php Icon Badge](../images/infos/icons/php.svg)                                     |
|    73 |           postgresql |        2,348 | ![postgresql Icon Badge](../images/infos/icons/postgresql.svg)                       |
|    74 |           powershell |          828 | ![powershell Icon Badge](../images/infos/icons/powershell.svg)                       |
|    75 |                 pypi |        2,552 | ![pypi Icon Badge](../images/infos/icons/pypi.svg)                                   |
|    76 |               python |        2,040 | ![python Icon Badge](../images/infos/icons/python.svg)                               |
|    77 |                  rss |          580 | ![rss Icon Badge](../images/infos/icons/rss.svg)                                     |
|    78 |                 ruby |          596 | ![ruby Icon Badge](../images/infos/icons/ruby.svg)                                   |
|    79 |          scrutinizer |        1,224 | ![scrutinizer Icon Badge](../images/infos/icons/scrutinizer.svg)                     |
|    80 |                slack |        1,180 | ![slack Icon Badge](../images/infos/icons/slack.svg)                                 |
|    81 |          sourceforge |        1,120 | ![sourceforge Icon Badge](../images/infos/icons/sourceforge.svg)                     |
|    82 |          sourcegraph |          964 | ![sourcegraph Icon Badge](../images/infos/icons/sourcegraph.svg)                     |
|    83 |        stackoverflow |          588 | ![stackoverflow Icon Badge](../images/infos/icons/stackoverflow.svg)                 |
|    84 |                steam |        1,364 | ![steam Icon Badge](../images/infos/icons/steam.svg)                                 |
|    85 |             telegram |          904 | ![telegram Icon Badge](../images/infos/icons/telegram.svg)                           |
|    86 |             terminal |          364 | ![terminal Icon Badge](../images/infos/icons/terminal.svg)                           |
|    87 |                  tor |        3,604 | ![tor Icon Badge](../images/infos/icons/tor.svg)                                     |
|    88 |               travis |        7,544 | ![travis Icon Badge](../images/infos/icons/travis.svg)                               |
|    89 |              twitter |          884 | ![twitter Icon Badge](../images/infos/icons/twitter.svg)                             |
|    90 |           typescript |          796 | ![typescript Icon Badge](../images/infos/icons/typescript.svg)                       |
|    91 |                unity |          416 | ![unity Icon Badge](../images/infos/icons/unity.svg)                                 |
|    92 |     visualstudiocode |          608 | ![visualstudiocode Icon Badge](../images/infos/icons/visualstudiocode.svg)           |
|    93 |       vlcmediaplayer |        1,024 | ![vlcmediaplayer Icon Badge](../images/infos/icons/vlcmediaplayer.svg)               |
|    94 |            wikipedia |        1,856 | ![wikipedia Icon Badge](../images/infos/icons/wikipedia.svg)                         |
|    95 |              windows |          384 | ![windows Icon Badge](../images/infos/icons/windows.svg)                             |
|    96 |              youtube |          644 | ![youtube Icon Badge](../images/infos/icons/youtube.svg)                             |
|    97 |                 zeit |          444 | ![zeit Icon Badge](../images/infos/icons/zeit.svg)                                   |
