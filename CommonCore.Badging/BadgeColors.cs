﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion
// <auto-generated />

using System;
using System.Collections.Generic;
using System.Drawing;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeColors" /> <c>static</c> <c>data</c> <c>class</c>
    /// provides access to built-in badge colors.
    /// Colors: 147
    /// </summary>
    public static class BadgeColors
    {
        /// <summary>
        /// Gets the Default badge color. HEX (RGB): #555555 (85, 85, 85)
        /// </summary>
        public static Color Default { get; } = Color.FromArgb(85, 85, 85);

        /// <summary>
        /// Gets the BGreen badge color. HEX (RGB): #33CC11 (51, 204, 17)
        /// </summary>
        public static Color BGreen { get; } = Color.FromArgb(51, 204, 17);

        /// <summary>
        /// Gets the BBlue badge color. HEX (RGB): #0088CC (0, 136, 204)
        /// </summary>
        public static Color BBlue { get; } = Color.FromArgb(0, 136, 204);

        /// <summary>
        /// Gets the BYellow badge color. HEX (RGB): #DDBB11 (221, 187, 17)
        /// </summary>
        public static Color BYellow { get; } = Color.FromArgb(221, 187, 17);

        /// <summary>
        /// Gets the BOrange badge color. HEX (RGB): #FF7733 (255, 119, 51)
        /// </summary>
        public static Color BOrange { get; } = Color.FromArgb(255, 119, 51);

        /// <summary>
        /// Gets the BRed badge color. HEX (RGB): #EE4433 (238, 68, 51)
        /// </summary>
        public static Color BRed { get; } = Color.FromArgb(238, 68, 51);

        /// <summary>
        /// Gets the BLightGray badge color. HEX (RGB): #999999 (153, 153, 153)
        /// </summary>
        public static Color BLightGray { get; } = Color.FromArgb(153, 153, 153);

        /// <summary>
        /// Gets the BDarkGray badge color. HEX (RGB): #555555 (85, 85, 85)
        /// </summary>
        public static Color BDarkGray { get; } = Color.FromArgb(85, 85, 85);

        /// <summary>
        /// Gets the AliceBlue badge color. HEX (RGB): #F0F8FF (240, 248, 255)
        /// </summary>
        public static Color AliceBlue { get; } = Color.FromArgb(240, 248, 255);

        /// <summary>
        /// Gets the AntiqueWhite badge color. HEX (RGB): #FAEBD7 (250, 235, 215)
        /// </summary>
        public static Color AntiqueWhite { get; } = Color.FromArgb(250, 235, 215);

        /// <summary>
        /// Gets the Aqua badge color. HEX (RGB): #00FFFF (0, 255, 255)
        /// </summary>
        public static Color Aqua { get; } = Color.FromArgb(0, 255, 255);

        /// <summary>
        /// Gets the Aquamarine badge color. HEX (RGB): #7FFFD4 (127, 255, 212)
        /// </summary>
        public static Color Aquamarine { get; } = Color.FromArgb(127, 255, 212);

        /// <summary>
        /// Gets the Azure badge color. HEX (RGB): #F0FFFF (240, 255, 255)
        /// </summary>
        public static Color Azure { get; } = Color.FromArgb(240, 255, 255);

        /// <summary>
        /// Gets the Beige badge color. HEX (RGB): #F5F5DC (245, 245, 220)
        /// </summary>
        public static Color Beige { get; } = Color.FromArgb(245, 245, 220);

        /// <summary>
        /// Gets the Bisque badge color. HEX (RGB): #FFE4C4 (255, 228, 196)
        /// </summary>
        public static Color Bisque { get; } = Color.FromArgb(255, 228, 196);

        /// <summary>
        /// Gets the Black badge color. HEX (RGB): #000000 (0, 0, 0)
        /// </summary>
        public static Color Black { get; } = Color.FromArgb(0, 0, 0);

        /// <summary>
        /// Gets the BlanchedAlmond badge color. HEX (RGB): #FFEBCD (255, 235, 205)
        /// </summary>
        public static Color BlanchedAlmond { get; } = Color.FromArgb(255, 235, 205);

        /// <summary>
        /// Gets the Blue badge color. HEX (RGB): #0000FF (0, 0, 255)
        /// </summary>
        public static Color Blue { get; } = Color.FromArgb(0, 0, 255);

        /// <summary>
        /// Gets the BlueViolet badge color. HEX (RGB): #8A2BE2 (138, 43, 226)
        /// </summary>
        public static Color BlueViolet { get; } = Color.FromArgb(138, 43, 226);

        /// <summary>
        /// Gets the Brown badge color. HEX (RGB): #A52A2A (165, 42, 42)
        /// </summary>
        public static Color Brown { get; } = Color.FromArgb(165, 42, 42);

        /// <summary>
        /// Gets the BurlyWood badge color. HEX (RGB): #DEB887 (222, 184, 135)
        /// </summary>
        public static Color BurlyWood { get; } = Color.FromArgb(222, 184, 135);

        /// <summary>
        /// Gets the CadetBlue badge color. HEX (RGB): #5F9EA0 (95, 158, 160)
        /// </summary>
        public static Color CadetBlue { get; } = Color.FromArgb(95, 158, 160);

        /// <summary>
        /// Gets the Chartreuse badge color. HEX (RGB): #7FFF00 (127, 255, 0)
        /// </summary>
        public static Color Chartreuse { get; } = Color.FromArgb(127, 255, 0);

        /// <summary>
        /// Gets the Chocolate badge color. HEX (RGB): #D2691E (210, 105, 30)
        /// </summary>
        public static Color Chocolate { get; } = Color.FromArgb(210, 105, 30);

        /// <summary>
        /// Gets the Coral badge color. HEX (RGB): #FF7F50 (255, 127, 80)
        /// </summary>
        public static Color Coral { get; } = Color.FromArgb(255, 127, 80);

        /// <summary>
        /// Gets the CornflowerBlue badge color. HEX (RGB): #6495ED (100, 149, 237)
        /// </summary>
        public static Color CornflowerBlue { get; } = Color.FromArgb(100, 149, 237);

        /// <summary>
        /// Gets the Cornsilk badge color. HEX (RGB): #FFF8DC (255, 248, 220)
        /// </summary>
        public static Color Cornsilk { get; } = Color.FromArgb(255, 248, 220);

        /// <summary>
        /// Gets the Crimson badge color. HEX (RGB): #DC143C (220, 20, 60)
        /// </summary>
        public static Color Crimson { get; } = Color.FromArgb(220, 20, 60);

        /// <summary>
        /// Gets the Cyan badge color. HEX (RGB): #00FFFF (0, 255, 255)
        /// </summary>
        public static Color Cyan { get; } = Color.FromArgb(0, 255, 255);

        /// <summary>
        /// Gets the DarkBlue badge color. HEX (RGB): #00008B (0, 0, 139)
        /// </summary>
        public static Color DarkBlue { get; } = Color.FromArgb(0, 0, 139);

        /// <summary>
        /// Gets the DarkCyan badge color. HEX (RGB): #008B8B (0, 139, 139)
        /// </summary>
        public static Color DarkCyan { get; } = Color.FromArgb(0, 139, 139);

        /// <summary>
        /// Gets the DarkGoldenrod badge color. HEX (RGB): #B8860B (184, 134, 11)
        /// </summary>
        public static Color DarkGoldenrod { get; } = Color.FromArgb(184, 134, 11);

        /// <summary>
        /// Gets the DarkGray badge color. HEX (RGB): #A9A9A9 (169, 169, 169)
        /// </summary>
        public static Color DarkGray { get; } = Color.FromArgb(169, 169, 169);

        /// <summary>
        /// Gets the DarkGreen badge color. HEX (RGB): #006400 (0, 100, 0)
        /// </summary>
        public static Color DarkGreen { get; } = Color.FromArgb(0, 100, 0);

        /// <summary>
        /// Gets the DarkKhaki badge color. HEX (RGB): #BDB76B (189, 183, 107)
        /// </summary>
        public static Color DarkKhaki { get; } = Color.FromArgb(189, 183, 107);

        /// <summary>
        /// Gets the DarkMagenta badge color. HEX (RGB): #8B008B (139, 0, 139)
        /// </summary>
        public static Color DarkMagenta { get; } = Color.FromArgb(139, 0, 139);

        /// <summary>
        /// Gets the DarkOliveGreen badge color. HEX (RGB): #556B2F (85, 107, 47)
        /// </summary>
        public static Color DarkOliveGreen { get; } = Color.FromArgb(85, 107, 47);

        /// <summary>
        /// Gets the DarkOrange badge color. HEX (RGB): #FF8C00 (255, 140, 0)
        /// </summary>
        public static Color DarkOrange { get; } = Color.FromArgb(255, 140, 0);

        /// <summary>
        /// Gets the DarkOrchid badge color. HEX (RGB): #9932CC (153, 50, 204)
        /// </summary>
        public static Color DarkOrchid { get; } = Color.FromArgb(153, 50, 204);

        /// <summary>
        /// Gets the DarkRed badge color. HEX (RGB): #8B0000 (139, 0, 0)
        /// </summary>
        public static Color DarkRed { get; } = Color.FromArgb(139, 0, 0);

        /// <summary>
        /// Gets the DarkSalmon badge color. HEX (RGB): #E9967A (233, 150, 122)
        /// </summary>
        public static Color DarkSalmon { get; } = Color.FromArgb(233, 150, 122);

        /// <summary>
        /// Gets the DarkSeaGreen badge color. HEX (RGB): #8FBC8B (143, 188, 139)
        /// </summary>
        public static Color DarkSeaGreen { get; } = Color.FromArgb(143, 188, 139);

        /// <summary>
        /// Gets the DarkSlateBlue badge color. HEX (RGB): #483D8B (72, 61, 139)
        /// </summary>
        public static Color DarkSlateBlue { get; } = Color.FromArgb(72, 61, 139);

        /// <summary>
        /// Gets the DarkSlateGray badge color. HEX (RGB): #2F4F4F (47, 79, 79)
        /// </summary>
        public static Color DarkSlateGray { get; } = Color.FromArgb(47, 79, 79);

        /// <summary>
        /// Gets the DarkTurquoise badge color. HEX (RGB): #00CED1 (0, 206, 209)
        /// </summary>
        public static Color DarkTurquoise { get; } = Color.FromArgb(0, 206, 209);

        /// <summary>
        /// Gets the DarkViolet badge color. HEX (RGB): #9400D3 (148, 0, 211)
        /// </summary>
        public static Color DarkViolet { get; } = Color.FromArgb(148, 0, 211);

        /// <summary>
        /// Gets the DeepPink badge color. HEX (RGB): #FF1493 (255, 20, 147)
        /// </summary>
        public static Color DeepPink { get; } = Color.FromArgb(255, 20, 147);

        /// <summary>
        /// Gets the DeepSkyBlue badge color. HEX (RGB): #00BFFF (0, 191, 255)
        /// </summary>
        public static Color DeepSkyBlue { get; } = Color.FromArgb(0, 191, 255);

        /// <summary>
        /// Gets the DimGray badge color. HEX (RGB): #696969 (105, 105, 105)
        /// </summary>
        public static Color DimGray { get; } = Color.FromArgb(105, 105, 105);

        /// <summary>
        /// Gets the DodgerBlue badge color. HEX (RGB): #1E90FF (30, 144, 255)
        /// </summary>
        public static Color DodgerBlue { get; } = Color.FromArgb(30, 144, 255);

        /// <summary>
        /// Gets the Firebrick badge color. HEX (RGB): #B22222 (178, 34, 34)
        /// </summary>
        public static Color Firebrick { get; } = Color.FromArgb(178, 34, 34);

        /// <summary>
        /// Gets the FloralWhite badge color. HEX (RGB): #FFFAF0 (255, 250, 240)
        /// </summary>
        public static Color FloralWhite { get; } = Color.FromArgb(255, 250, 240);

        /// <summary>
        /// Gets the ForestGreen badge color. HEX (RGB): #228B22 (34, 139, 34)
        /// </summary>
        public static Color ForestGreen { get; } = Color.FromArgb(34, 139, 34);

        /// <summary>
        /// Gets the Fuchsia badge color. HEX (RGB): #FF00FF (255, 0, 255)
        /// </summary>
        public static Color Fuchsia { get; } = Color.FromArgb(255, 0, 255);

        /// <summary>
        /// Gets the Gainsboro badge color. HEX (RGB): #DCDCDC (220, 220, 220)
        /// </summary>
        public static Color Gainsboro { get; } = Color.FromArgb(220, 220, 220);

        /// <summary>
        /// Gets the GhostWhite badge color. HEX (RGB): #F8F8FF (248, 248, 255)
        /// </summary>
        public static Color GhostWhite { get; } = Color.FromArgb(248, 248, 255);

        /// <summary>
        /// Gets the Gold badge color. HEX (RGB): #FFD700 (255, 215, 0)
        /// </summary>
        public static Color Gold { get; } = Color.FromArgb(255, 215, 0);

        /// <summary>
        /// Gets the Goldenrod badge color. HEX (RGB): #DAA520 (218, 165, 32)
        /// </summary>
        public static Color Goldenrod { get; } = Color.FromArgb(218, 165, 32);

        /// <summary>
        /// Gets the Gray badge color. HEX (RGB): #808080 (128, 128, 128)
        /// </summary>
        public static Color Gray { get; } = Color.FromArgb(128, 128, 128);

        /// <summary>
        /// Gets the Green badge color. HEX (RGB): #008000 (0, 128, 0)
        /// </summary>
        public static Color Green { get; } = Color.FromArgb(0, 128, 0);

        /// <summary>
        /// Gets the GreenYellow badge color. HEX (RGB): #ADFF2F (173, 255, 47)
        /// </summary>
        public static Color GreenYellow { get; } = Color.FromArgb(173, 255, 47);

        /// <summary>
        /// Gets the Honeydew badge color. HEX (RGB): #F0FFF0 (240, 255, 240)
        /// </summary>
        public static Color Honeydew { get; } = Color.FromArgb(240, 255, 240);

        /// <summary>
        /// Gets the HotPink badge color. HEX (RGB): #FF69B4 (255, 105, 180)
        /// </summary>
        public static Color HotPink { get; } = Color.FromArgb(255, 105, 180);

        /// <summary>
        /// Gets the IndianRed badge color. HEX (RGB): #CD5C5C (205, 92, 92)
        /// </summary>
        public static Color IndianRed { get; } = Color.FromArgb(205, 92, 92);

        /// <summary>
        /// Gets the Indigo badge color. HEX (RGB): #4B0082 (75, 0, 130)
        /// </summary>
        public static Color Indigo { get; } = Color.FromArgb(75, 0, 130);

        /// <summary>
        /// Gets the Ivory badge color. HEX (RGB): #FFFFF0 (255, 255, 240)
        /// </summary>
        public static Color Ivory { get; } = Color.FromArgb(255, 255, 240);

        /// <summary>
        /// Gets the Khaki badge color. HEX (RGB): #F0E68C (240, 230, 140)
        /// </summary>
        public static Color Khaki { get; } = Color.FromArgb(240, 230, 140);

        /// <summary>
        /// Gets the Lavender badge color. HEX (RGB): #E6E6FA (230, 230, 250)
        /// </summary>
        public static Color Lavender { get; } = Color.FromArgb(230, 230, 250);

        /// <summary>
        /// Gets the LavenderBlush badge color. HEX (RGB): #FFF0F5 (255, 240, 245)
        /// </summary>
        public static Color LavenderBlush { get; } = Color.FromArgb(255, 240, 245);

        /// <summary>
        /// Gets the LawnGreen badge color. HEX (RGB): #7CFC00 (124, 252, 0)
        /// </summary>
        public static Color LawnGreen { get; } = Color.FromArgb(124, 252, 0);

        /// <summary>
        /// Gets the LemonChiffon badge color. HEX (RGB): #FFFACD (255, 250, 205)
        /// </summary>
        public static Color LemonChiffon { get; } = Color.FromArgb(255, 250, 205);

        /// <summary>
        /// Gets the LightBlue badge color. HEX (RGB): #ADD8E6 (173, 216, 230)
        /// </summary>
        public static Color LightBlue { get; } = Color.FromArgb(173, 216, 230);

        /// <summary>
        /// Gets the LightCoral badge color. HEX (RGB): #F08080 (240, 128, 128)
        /// </summary>
        public static Color LightCoral { get; } = Color.FromArgb(240, 128, 128);

        /// <summary>
        /// Gets the LightCyan badge color. HEX (RGB): #E0FFFF (224, 255, 255)
        /// </summary>
        public static Color LightCyan { get; } = Color.FromArgb(224, 255, 255);

        /// <summary>
        /// Gets the LightGoldenrodYellow badge color. HEX (RGB): #FAFAD2 (250, 250, 210)
        /// </summary>
        public static Color LightGoldenrodYellow { get; } = Color.FromArgb(250, 250, 210);

        /// <summary>
        /// Gets the LightGray badge color. HEX (RGB): #D3D3D3 (211, 211, 211)
        /// </summary>
        public static Color LightGray { get; } = Color.FromArgb(211, 211, 211);

        /// <summary>
        /// Gets the LightGreen badge color. HEX (RGB): #90EE90 (144, 238, 144)
        /// </summary>
        public static Color LightGreen { get; } = Color.FromArgb(144, 238, 144);

        /// <summary>
        /// Gets the LightPink badge color. HEX (RGB): #FFB6C1 (255, 182, 193)
        /// </summary>
        public static Color LightPink { get; } = Color.FromArgb(255, 182, 193);

        /// <summary>
        /// Gets the LightSalmon badge color. HEX (RGB): #FFA07A (255, 160, 122)
        /// </summary>
        public static Color LightSalmon { get; } = Color.FromArgb(255, 160, 122);

        /// <summary>
        /// Gets the LightSeaGreen badge color. HEX (RGB): #20B2AA (32, 178, 170)
        /// </summary>
        public static Color LightSeaGreen { get; } = Color.FromArgb(32, 178, 170);

        /// <summary>
        /// Gets the LightSkyBlue badge color. HEX (RGB): #87CEFA (135, 206, 250)
        /// </summary>
        public static Color LightSkyBlue { get; } = Color.FromArgb(135, 206, 250);

        /// <summary>
        /// Gets the LightSlateGray badge color. HEX (RGB): #778899 (119, 136, 153)
        /// </summary>
        public static Color LightSlateGray { get; } = Color.FromArgb(119, 136, 153);

        /// <summary>
        /// Gets the LightSteelBlue badge color. HEX (RGB): #B0C4DE (176, 196, 222)
        /// </summary>
        public static Color LightSteelBlue { get; } = Color.FromArgb(176, 196, 222);

        /// <summary>
        /// Gets the LightYellow badge color. HEX (RGB): #FFFFE0 (255, 255, 224)
        /// </summary>
        public static Color LightYellow { get; } = Color.FromArgb(255, 255, 224);

        /// <summary>
        /// Gets the Lime badge color. HEX (RGB): #00FF00 (0, 255, 0)
        /// </summary>
        public static Color Lime { get; } = Color.FromArgb(0, 255, 0);

        /// <summary>
        /// Gets the LimeGreen badge color. HEX (RGB): #32CD32 (50, 205, 50)
        /// </summary>
        public static Color LimeGreen { get; } = Color.FromArgb(50, 205, 50);

        /// <summary>
        /// Gets the Linen badge color. HEX (RGB): #FAF0E6 (250, 240, 230)
        /// </summary>
        public static Color Linen { get; } = Color.FromArgb(250, 240, 230);

        /// <summary>
        /// Gets the Magenta badge color. HEX (RGB): #FF00FF (255, 0, 255)
        /// </summary>
        public static Color Magenta { get; } = Color.FromArgb(255, 0, 255);

        /// <summary>
        /// Gets the Maroon badge color. HEX (RGB): #800000 (128, 0, 0)
        /// </summary>
        public static Color Maroon { get; } = Color.FromArgb(128, 0, 0);

        /// <summary>
        /// Gets the MediumAquamarine badge color. HEX (RGB): #66CDAA (102, 205, 170)
        /// </summary>
        public static Color MediumAquamarine { get; } = Color.FromArgb(102, 205, 170);

        /// <summary>
        /// Gets the MediumBlue badge color. HEX (RGB): #0000CD (0, 0, 205)
        /// </summary>
        public static Color MediumBlue { get; } = Color.FromArgb(0, 0, 205);

        /// <summary>
        /// Gets the MediumOrchid badge color. HEX (RGB): #BA55D3 (186, 85, 211)
        /// </summary>
        public static Color MediumOrchid { get; } = Color.FromArgb(186, 85, 211);

        /// <summary>
        /// Gets the MediumPurple badge color. HEX (RGB): #9370DB (147, 112, 219)
        /// </summary>
        public static Color MediumPurple { get; } = Color.FromArgb(147, 112, 219);

        /// <summary>
        /// Gets the MediumSeaGreen badge color. HEX (RGB): #3CB371 (60, 179, 113)
        /// </summary>
        public static Color MediumSeaGreen { get; } = Color.FromArgb(60, 179, 113);

        /// <summary>
        /// Gets the MediumSlateBlue badge color. HEX (RGB): #7B68EE (123, 104, 238)
        /// </summary>
        public static Color MediumSlateBlue { get; } = Color.FromArgb(123, 104, 238);

        /// <summary>
        /// Gets the MediumSpringGreen badge color. HEX (RGB): #00FA9A (0, 250, 154)
        /// </summary>
        public static Color MediumSpringGreen { get; } = Color.FromArgb(0, 250, 154);

        /// <summary>
        /// Gets the MediumTurquoise badge color. HEX (RGB): #48D1CC (72, 209, 204)
        /// </summary>
        public static Color MediumTurquoise { get; } = Color.FromArgb(72, 209, 204);

        /// <summary>
        /// Gets the MediumVioletRed badge color. HEX (RGB): #C71585 (199, 21, 133)
        /// </summary>
        public static Color MediumVioletRed { get; } = Color.FromArgb(199, 21, 133);

        /// <summary>
        /// Gets the MidnightBlue badge color. HEX (RGB): #191970 (25, 25, 112)
        /// </summary>
        public static Color MidnightBlue { get; } = Color.FromArgb(25, 25, 112);

        /// <summary>
        /// Gets the MintCream badge color. HEX (RGB): #F5FFFA (245, 255, 250)
        /// </summary>
        public static Color MintCream { get; } = Color.FromArgb(245, 255, 250);

        /// <summary>
        /// Gets the MistyRose badge color. HEX (RGB): #FFE4E1 (255, 228, 225)
        /// </summary>
        public static Color MistyRose { get; } = Color.FromArgb(255, 228, 225);

        /// <summary>
        /// Gets the Moccasin badge color. HEX (RGB): #FFE4B5 (255, 228, 181)
        /// </summary>
        public static Color Moccasin { get; } = Color.FromArgb(255, 228, 181);

        /// <summary>
        /// Gets the NavajoWhite badge color. HEX (RGB): #FFDEAD (255, 222, 173)
        /// </summary>
        public static Color NavajoWhite { get; } = Color.FromArgb(255, 222, 173);

        /// <summary>
        /// Gets the Navy badge color. HEX (RGB): #000080 (0, 0, 128)
        /// </summary>
        public static Color Navy { get; } = Color.FromArgb(0, 0, 128);

        /// <summary>
        /// Gets the OldLace badge color. HEX (RGB): #FDF5E6 (253, 245, 230)
        /// </summary>
        public static Color OldLace { get; } = Color.FromArgb(253, 245, 230);

        /// <summary>
        /// Gets the Olive badge color. HEX (RGB): #808000 (128, 128, 0)
        /// </summary>
        public static Color Olive { get; } = Color.FromArgb(128, 128, 0);

        /// <summary>
        /// Gets the OliveDrab badge color. HEX (RGB): #6B8E23 (107, 142, 35)
        /// </summary>
        public static Color OliveDrab { get; } = Color.FromArgb(107, 142, 35);

        /// <summary>
        /// Gets the Orange badge color. HEX (RGB): #FFA500 (255, 165, 0)
        /// </summary>
        public static Color Orange { get; } = Color.FromArgb(255, 165, 0);

        /// <summary>
        /// Gets the OrangeRed badge color. HEX (RGB): #FF4500 (255, 69, 0)
        /// </summary>
        public static Color OrangeRed { get; } = Color.FromArgb(255, 69, 0);

        /// <summary>
        /// Gets the Orchid badge color. HEX (RGB): #DA70D6 (218, 112, 214)
        /// </summary>
        public static Color Orchid { get; } = Color.FromArgb(218, 112, 214);

        /// <summary>
        /// Gets the PaleGoldenrod badge color. HEX (RGB): #EEE8AA (238, 232, 170)
        /// </summary>
        public static Color PaleGoldenrod { get; } = Color.FromArgb(238, 232, 170);

        /// <summary>
        /// Gets the PaleGreen badge color. HEX (RGB): #98FB98 (152, 251, 152)
        /// </summary>
        public static Color PaleGreen { get; } = Color.FromArgb(152, 251, 152);

        /// <summary>
        /// Gets the PaleTurquoise badge color. HEX (RGB): #AFEEEE (175, 238, 238)
        /// </summary>
        public static Color PaleTurquoise { get; } = Color.FromArgb(175, 238, 238);

        /// <summary>
        /// Gets the PaleVioletRed badge color. HEX (RGB): #DB7093 (219, 112, 147)
        /// </summary>
        public static Color PaleVioletRed { get; } = Color.FromArgb(219, 112, 147);

        /// <summary>
        /// Gets the PapayaWhip badge color. HEX (RGB): #FFEFD5 (255, 239, 213)
        /// </summary>
        public static Color PapayaWhip { get; } = Color.FromArgb(255, 239, 213);

        /// <summary>
        /// Gets the PeachPuff badge color. HEX (RGB): #FFDAB9 (255, 218, 185)
        /// </summary>
        public static Color PeachPuff { get; } = Color.FromArgb(255, 218, 185);

        /// <summary>
        /// Gets the Peru badge color. HEX (RGB): #CD853F (205, 133, 63)
        /// </summary>
        public static Color Peru { get; } = Color.FromArgb(205, 133, 63);

        /// <summary>
        /// Gets the Pink badge color. HEX (RGB): #FFC0CB (255, 192, 203)
        /// </summary>
        public static Color Pink { get; } = Color.FromArgb(255, 192, 203);

        /// <summary>
        /// Gets the Plum badge color. HEX (RGB): #DDA0DD (221, 160, 221)
        /// </summary>
        public static Color Plum { get; } = Color.FromArgb(221, 160, 221);

        /// <summary>
        /// Gets the PowderBlue badge color. HEX (RGB): #B0E0E6 (176, 224, 230)
        /// </summary>
        public static Color PowderBlue { get; } = Color.FromArgb(176, 224, 230);

        /// <summary>
        /// Gets the Purple badge color. HEX (RGB): #800080 (128, 0, 128)
        /// </summary>
        public static Color Purple { get; } = Color.FromArgb(128, 0, 128);

        /// <summary>
        /// Gets the Red badge color. HEX (RGB): #FF0000 (255, 0, 0)
        /// </summary>
        public static Color Red { get; } = Color.FromArgb(255, 0, 0);

        /// <summary>
        /// Gets the RosyBrown badge color. HEX (RGB): #BC8F8F (188, 143, 143)
        /// </summary>
        public static Color RosyBrown { get; } = Color.FromArgb(188, 143, 143);

        /// <summary>
        /// Gets the RoyalBlue badge color. HEX (RGB): #4169E1 (65, 105, 225)
        /// </summary>
        public static Color RoyalBlue { get; } = Color.FromArgb(65, 105, 225);

        /// <summary>
        /// Gets the SaddleBrown badge color. HEX (RGB): #8B4513 (139, 69, 19)
        /// </summary>
        public static Color SaddleBrown { get; } = Color.FromArgb(139, 69, 19);

        /// <summary>
        /// Gets the Salmon badge color. HEX (RGB): #FA8072 (250, 128, 114)
        /// </summary>
        public static Color Salmon { get; } = Color.FromArgb(250, 128, 114);

        /// <summary>
        /// Gets the SandyBrown badge color. HEX (RGB): #F4A460 (244, 164, 96)
        /// </summary>
        public static Color SandyBrown { get; } = Color.FromArgb(244, 164, 96);

        /// <summary>
        /// Gets the SeaGreen badge color. HEX (RGB): #2E8B57 (46, 139, 87)
        /// </summary>
        public static Color SeaGreen { get; } = Color.FromArgb(46, 139, 87);

        /// <summary>
        /// Gets the SeaShell badge color. HEX (RGB): #FFF5EE (255, 245, 238)
        /// </summary>
        public static Color SeaShell { get; } = Color.FromArgb(255, 245, 238);

        /// <summary>
        /// Gets the Sienna badge color. HEX (RGB): #A0522D (160, 82, 45)
        /// </summary>
        public static Color Sienna { get; } = Color.FromArgb(160, 82, 45);

        /// <summary>
        /// Gets the Silver badge color. HEX (RGB): #C0C0C0 (192, 192, 192)
        /// </summary>
        public static Color Silver { get; } = Color.FromArgb(192, 192, 192);

        /// <summary>
        /// Gets the SkyBlue badge color. HEX (RGB): #87CEEB (135, 206, 235)
        /// </summary>
        public static Color SkyBlue { get; } = Color.FromArgb(135, 206, 235);

        /// <summary>
        /// Gets the SlateBlue badge color. HEX (RGB): #6A5ACD (106, 90, 205)
        /// </summary>
        public static Color SlateBlue { get; } = Color.FromArgb(106, 90, 205);

        /// <summary>
        /// Gets the SlateGray badge color. HEX (RGB): #708090 (112, 128, 144)
        /// </summary>
        public static Color SlateGray { get; } = Color.FromArgb(112, 128, 144);

        /// <summary>
        /// Gets the Snow badge color. HEX (RGB): #FFFAFA (255, 250, 250)
        /// </summary>
        public static Color Snow { get; } = Color.FromArgb(255, 250, 250);

        /// <summary>
        /// Gets the SpringGreen badge color. HEX (RGB): #00FF7F (0, 255, 127)
        /// </summary>
        public static Color SpringGreen { get; } = Color.FromArgb(0, 255, 127);

        /// <summary>
        /// Gets the SteelBlue badge color. HEX (RGB): #4682B4 (70, 130, 180)
        /// </summary>
        public static Color SteelBlue { get; } = Color.FromArgb(70, 130, 180);

        /// <summary>
        /// Gets the Tan badge color. HEX (RGB): #D2B48C (210, 180, 140)
        /// </summary>
        public static Color Tan { get; } = Color.FromArgb(210, 180, 140);

        /// <summary>
        /// Gets the Teal badge color. HEX (RGB): #008080 (0, 128, 128)
        /// </summary>
        public static Color Teal { get; } = Color.FromArgb(0, 128, 128);

        /// <summary>
        /// Gets the Thistle badge color. HEX (RGB): #D8BFD8 (216, 191, 216)
        /// </summary>
        public static Color Thistle { get; } = Color.FromArgb(216, 191, 216);

        /// <summary>
        /// Gets the Tomato badge color. HEX (RGB): #FF6347 (255, 99, 71)
        /// </summary>
        public static Color Tomato { get; } = Color.FromArgb(255, 99, 71);

        /// <summary>
        /// Gets the Turquoise badge color. HEX (RGB): #40E0D0 (64, 224, 208)
        /// </summary>
        public static Color Turquoise { get; } = Color.FromArgb(64, 224, 208);

        /// <summary>
        /// Gets the Violet badge color. HEX (RGB): #EE82EE (238, 130, 238)
        /// </summary>
        public static Color Violet { get; } = Color.FromArgb(238, 130, 238);

        /// <summary>
        /// Gets the Wheat badge color. HEX (RGB): #F5DEB3 (245, 222, 179)
        /// </summary>
        public static Color Wheat { get; } = Color.FromArgb(245, 222, 179);

        /// <summary>
        /// Gets the White badge color. HEX (RGB): #FFFFFF (255, 255, 255)
        /// </summary>
        public static Color White { get; } = Color.FromArgb(255, 255, 255);

        /// <summary>
        /// Gets the WhiteSmoke badge color. HEX (RGB): #F5F5F5 (245, 245, 245)
        /// </summary>
        public static Color WhiteSmoke { get; } = Color.FromArgb(245, 245, 245);

        /// <summary>
        /// Gets the Yellow badge color. HEX (RGB): #FFFF00 (255, 255, 0)
        /// </summary>
        public static Color Yellow { get; } = Color.FromArgb(255, 255, 0);

        /// <summary>
        /// Gets the YellowGreen badge color. HEX (RGB): #9ACD32 (154, 205, 50)
        /// </summary>
        public static Color YellowGreen { get; } = Color.FromArgb(154, 205, 50);

        /// <summary>
        /// Gets the dictionary of available badge colors.
        /// Case-insensitive indexing is used for the keys.
        /// </summary>
        public static Dictionary<string, Color> Dict { get; } = new Dictionary<string, Color>(StringComparer.InvariantCultureIgnoreCase)
        {
            { "", BadgeColors.Default },
            { "Default", BadgeColors.Default },
            { "BGreen", BadgeColors.BGreen },
            { "BBlue", BadgeColors.BBlue },
            { "BYellow", BadgeColors.BYellow },
            { "BOrange", BadgeColors.BOrange },
            { "BRed", BadgeColors.BRed },
            { "BLightGray", BadgeColors.BLightGray },
            { "BDarkGray", BadgeColors.BDarkGray },
            { "AliceBlue", BadgeColors.AliceBlue },
            { "AntiqueWhite", BadgeColors.AntiqueWhite },
            { "Aqua", BadgeColors.Aqua },
            { "Aquamarine", BadgeColors.Aquamarine },
            { "Azure", BadgeColors.Azure },
            { "Beige", BadgeColors.Beige },
            { "Bisque", BadgeColors.Bisque },
            { "Black", BadgeColors.Black },
            { "BlanchedAlmond", BadgeColors.BlanchedAlmond },
            { "Blue", BadgeColors.Blue },
            { "BlueViolet", BadgeColors.BlueViolet },
            { "Brown", BadgeColors.Brown },
            { "BurlyWood", BadgeColors.BurlyWood },
            { "CadetBlue", BadgeColors.CadetBlue },
            { "Chartreuse", BadgeColors.Chartreuse },
            { "Chocolate", BadgeColors.Chocolate },
            { "Coral", BadgeColors.Coral },
            { "CornflowerBlue", BadgeColors.CornflowerBlue },
            { "Cornsilk", BadgeColors.Cornsilk },
            { "Crimson", BadgeColors.Crimson },
            { "Cyan", BadgeColors.Cyan },
            { "DarkBlue", BadgeColors.DarkBlue },
            { "DarkCyan", BadgeColors.DarkCyan },
            { "DarkGoldenrod", BadgeColors.DarkGoldenrod },
            { "DarkGray", BadgeColors.DarkGray },
            { "DarkGreen", BadgeColors.DarkGreen },
            { "DarkKhaki", BadgeColors.DarkKhaki },
            { "DarkMagenta", BadgeColors.DarkMagenta },
            { "DarkOliveGreen", BadgeColors.DarkOliveGreen },
            { "DarkOrange", BadgeColors.DarkOrange },
            { "DarkOrchid", BadgeColors.DarkOrchid },
            { "DarkRed", BadgeColors.DarkRed },
            { "DarkSalmon", BadgeColors.DarkSalmon },
            { "DarkSeaGreen", BadgeColors.DarkSeaGreen },
            { "DarkSlateBlue", BadgeColors.DarkSlateBlue },
            { "DarkSlateGray", BadgeColors.DarkSlateGray },
            { "DarkTurquoise", BadgeColors.DarkTurquoise },
            { "DarkViolet", BadgeColors.DarkViolet },
            { "DeepPink", BadgeColors.DeepPink },
            { "DeepSkyBlue", BadgeColors.DeepSkyBlue },
            { "DimGray", BadgeColors.DimGray },
            { "DodgerBlue", BadgeColors.DodgerBlue },
            { "Firebrick", BadgeColors.Firebrick },
            { "FloralWhite", BadgeColors.FloralWhite },
            { "ForestGreen", BadgeColors.ForestGreen },
            { "Fuchsia", BadgeColors.Fuchsia },
            { "Gainsboro", BadgeColors.Gainsboro },
            { "GhostWhite", BadgeColors.GhostWhite },
            { "Gold", BadgeColors.Gold },
            { "Goldenrod", BadgeColors.Goldenrod },
            { "Gray", BadgeColors.Gray },
            { "Green", BadgeColors.Green },
            { "GreenYellow", BadgeColors.GreenYellow },
            { "Honeydew", BadgeColors.Honeydew },
            { "HotPink", BadgeColors.HotPink },
            { "IndianRed", BadgeColors.IndianRed },
            { "Indigo", BadgeColors.Indigo },
            { "Ivory", BadgeColors.Ivory },
            { "Khaki", BadgeColors.Khaki },
            { "Lavender", BadgeColors.Lavender },
            { "LavenderBlush", BadgeColors.LavenderBlush },
            { "LawnGreen", BadgeColors.LawnGreen },
            { "LemonChiffon", BadgeColors.LemonChiffon },
            { "LightBlue", BadgeColors.LightBlue },
            { "LightCoral", BadgeColors.LightCoral },
            { "LightCyan", BadgeColors.LightCyan },
            { "LightGoldenrodYellow", BadgeColors.LightGoldenrodYellow },
            { "LightGray", BadgeColors.LightGray },
            { "LightGreen", BadgeColors.LightGreen },
            { "LightPink", BadgeColors.LightPink },
            { "LightSalmon", BadgeColors.LightSalmon },
            { "LightSeaGreen", BadgeColors.LightSeaGreen },
            { "LightSkyBlue", BadgeColors.LightSkyBlue },
            { "LightSlateGray", BadgeColors.LightSlateGray },
            { "LightSteelBlue", BadgeColors.LightSteelBlue },
            { "LightYellow", BadgeColors.LightYellow },
            { "Lime", BadgeColors.Lime },
            { "LimeGreen", BadgeColors.LimeGreen },
            { "Linen", BadgeColors.Linen },
            { "Magenta", BadgeColors.Magenta },
            { "Maroon", BadgeColors.Maroon },
            { "MediumAquamarine", BadgeColors.MediumAquamarine },
            { "MediumBlue", BadgeColors.MediumBlue },
            { "MediumOrchid", BadgeColors.MediumOrchid },
            { "MediumPurple", BadgeColors.MediumPurple },
            { "MediumSeaGreen", BadgeColors.MediumSeaGreen },
            { "MediumSlateBlue", BadgeColors.MediumSlateBlue },
            { "MediumSpringGreen", BadgeColors.MediumSpringGreen },
            { "MediumTurquoise", BadgeColors.MediumTurquoise },
            { "MediumVioletRed", BadgeColors.MediumVioletRed },
            { "MidnightBlue", BadgeColors.MidnightBlue },
            { "MintCream", BadgeColors.MintCream },
            { "MistyRose", BadgeColors.MistyRose },
            { "Moccasin", BadgeColors.Moccasin },
            { "NavajoWhite", BadgeColors.NavajoWhite },
            { "Navy", BadgeColors.Navy },
            { "OldLace", BadgeColors.OldLace },
            { "Olive", BadgeColors.Olive },
            { "OliveDrab", BadgeColors.OliveDrab },
            { "Orange", BadgeColors.Orange },
            { "OrangeRed", BadgeColors.OrangeRed },
            { "Orchid", BadgeColors.Orchid },
            { "PaleGoldenrod", BadgeColors.PaleGoldenrod },
            { "PaleGreen", BadgeColors.PaleGreen },
            { "PaleTurquoise", BadgeColors.PaleTurquoise },
            { "PaleVioletRed", BadgeColors.PaleVioletRed },
            { "PapayaWhip", BadgeColors.PapayaWhip },
            { "PeachPuff", BadgeColors.PeachPuff },
            { "Peru", BadgeColors.Peru },
            { "Pink", BadgeColors.Pink },
            { "Plum", BadgeColors.Plum },
            { "PowderBlue", BadgeColors.PowderBlue },
            { "Purple", BadgeColors.Purple },
            { "Red", BadgeColors.Red },
            { "RosyBrown", BadgeColors.RosyBrown },
            { "RoyalBlue", BadgeColors.RoyalBlue },
            { "SaddleBrown", BadgeColors.SaddleBrown },
            { "Salmon", BadgeColors.Salmon },
            { "SandyBrown", BadgeColors.SandyBrown },
            { "SeaGreen", BadgeColors.SeaGreen },
            { "SeaShell", BadgeColors.SeaShell },
            { "Sienna", BadgeColors.Sienna },
            { "Silver", BadgeColors.Silver },
            { "SkyBlue", BadgeColors.SkyBlue },
            { "SlateBlue", BadgeColors.SlateBlue },
            { "SlateGray", BadgeColors.SlateGray },
            { "Snow", BadgeColors.Snow },
            { "SpringGreen", BadgeColors.SpringGreen },
            { "SteelBlue", BadgeColors.SteelBlue },
            { "Tan", BadgeColors.Tan },
            { "Teal", BadgeColors.Teal },
            { "Thistle", BadgeColors.Thistle },
            { "Tomato", BadgeColors.Tomato },
            { "Turquoise", BadgeColors.Turquoise },
            { "Violet", BadgeColors.Violet },
            { "Wheat", BadgeColors.Wheat },
            { "White", BadgeColors.White },
            { "WhiteSmoke", BadgeColors.WhiteSmoke },
            { "Yellow", BadgeColors.Yellow },
            { "YellowGreen", BadgeColors.YellowGreen }
        };
    }
}
