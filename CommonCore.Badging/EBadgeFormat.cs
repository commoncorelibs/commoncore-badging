﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="EBadgeFormat"/> <c>enum</c> specifies which format to use
    /// when converting <see cref="Badge"/> objects to <see cref="string"/> representations.
    /// </summary>
    public enum EBadgeFormat : int
    {
        /// <summary>
        /// Indicates the svg format.
        /// </summary>
        Svg,

        /// <summary>
        /// Indicates the base64 encoded svg format.
        /// </summary>
        Base64
    }
}
