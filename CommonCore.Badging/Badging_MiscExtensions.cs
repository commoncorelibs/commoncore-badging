﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Net;
using System.Text;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="Badging_MiscExtensions"/> <c>static</c> <c>class</c> provides
    /// miscellaneous extension methods used by the badging project.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
    public static class Badging_MiscExtensions
    {

        #region Deprecated Members

        /// <summary>
        /// Returns the <see cref="Color"/> converted to its six-digit hex (#000000) <see cref="string"/> representation.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="Color"/> converted to its six-digit hex (#000000) <see cref="string"/> representation.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static string ToHex(this Color self) => $"#{self.R:X2}{self.G:X2}{self.B:X2}";

        /// <summary>
        /// Returns the <see cref="Color"/> converted to its three-part comma-separated rgb (255,255,255) <see cref="string"/> representation.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="Color"/> converted to its three-part comma-separated rgb (255,255,255) <see cref="string"/> representation.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static string ToRgb(this Color self) => $"{self.R},{self.G},{self.B}";

        /// <summary>
        /// Returns the <see cref="string"/> converted to its base64 encoded <see cref="string"/> representation.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="string"/> converted to its base64 encoded <see cref="string"/> representation.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static string ToBase64(this string self) => Convert.ToBase64String(Encoding.UTF8.GetBytes(self));

        /// <summary>
        /// Downloads the specified <see cref="Uri"/> as a <see cref="string"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The specified <see cref="Uri"/> as a <see cref="string"/>.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static string DownloadString(this Uri self) { using var client = new WebClient(); return client.DownloadString(self); }

        /// <summary>
        /// Downloads the specified <see cref="Uri"/> as a <see cref="string"/> and encoded as base64.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The specified <see cref="Uri"/> as a <see cref="string"/> and encoded as base64.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static string DownloadBase64(this Uri self) => self.DownloadString().ToBase64();

        /// <summary>
        /// Throws <see cref="ArgumentNullException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">Type of the value.</typeparam>
        /// <param name="value">The value to check.</param>
        /// <param name="name">The name of the argument.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeUtils instead.", false)]
        public static void ThrowArgNull<T>(T value, string name) where T : class
        { if (value is null) { throw new ArgumentNullException(name); } }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="AppendSvgPath(StringBuilder, string, int, int, int)"/>
        /// instead and pass the hex string of the section color as the fill parameter.
        /// Appends a svg path with the section color as the fill.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="position">The upper-left x position of the path rectangle.</param>
        /// <param name="width">The width of the path rectangle.</param>
        /// <param name="height">The height of the path rectangle.</param>
        /// <param name="section">The section with the color of the path fill.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use string fill overload instead.", false)]
        public static void AppendSvgPath(this StringBuilder self, int position, int width, int height, BadgeSection section)
            => self.AppendSvgPath(section.Color.ToHex(), position, width, height);

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder"/> instead.
        /// Appends a svg path with the specified <paramref name="fill"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="fill">The fill value of the path.</param>
        /// <param name="position">The upper-left x position of the path rectangle.</param>
        /// <param name="width">The width of the path rectangle.</param>
        /// <param name="height">The height of the path rectangle.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder instead.", false)]
        public static void AppendSvgPath(this StringBuilder self, string fill, int position, int width, int height)
            => self.AppendLine($@"        <path fill=""{fill}"" d=""M{position} 0h{width}v{height}h-{width}z""/>");

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder"/> instead.
        /// Appends the appropriate svg elements for the badge section icon.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="position">The current horizontal (x) position on the section.</param>
        /// <param name="sectionWidth">The width of the current section.</param>
        /// <param name="sectionHeight">The height of the current section.</param>
        /// <param name="section">The current section.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder instead.", false)]
        public static void AppendSvgImage(this StringBuilder self, int position, int sectionWidth, int sectionHeight, BadgeSection section)
        {
            int iconWidth = section.CalIconWidth();
            int paddedWidth = (int)(iconWidth - (section.IconPadding * 2));
            int paddedHeight = (int)(sectionHeight - (section.IconPadding * 2));
            self.AppendLine($@"    <image x=""{position + (int)section.IconPadding}"" y=""{(int)section.IconPadding}"" width=""{paddedWidth}"" height=""{paddedHeight}"" viewBox=""0 0 {paddedWidth} {paddedHeight}"" xlink:href=""data:image/svg+xml;base64,{section.Icon}"" />");
        }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder"/> instead.
        /// Appends the appropriate svg elements for the badge section text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="position">The current horizontal (x) position on the section.</param>
        /// <param name="sectionWidth">The width of the current section.</param>
        /// <param name="sectionHeight">The height of the current section.</param>
        /// <param name="section">The current section.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder instead.", false)]
        public static void AppendSvgText(this StringBuilder self, int position, int sectionWidth, int sectionHeight, BadgeSection section)
        {
            var alignment = "middle";
            int x = position + (section.IconExists ? 20 + ((sectionWidth - 20) / 2) : sectionWidth / 2);

            var textSize = section.TextStyle.Font.CalTextSize(section.Text, section.TextStyle.FontSize);
            int y = (int)(sectionHeight - ((sectionHeight - textSize.Height) / 2)) - 3;
            self.AppendLine($@"    <g fill=""{section.TextStyle.Font.Color.ToHex()}"" text-anchor=""{alignment}"" font-family=""{section.TextStyle.Font.Name}"" font-size=""{section.TextStyle.FontSize}"">");
            if (section.EnableTextShadow)
            { self.AppendLine($@"        <text x=""{x + 1}"" y=""{y + 1}"" fill=""#010101"" fill-opacity="".3"">{section.Text}</text>"); }
            self.AppendLine($@"        <text x=""{x}"" y=""{y}"">{section.Text}</text>");
            self.AppendLine(@"    </g>");
        }

        #endregion
    }
}
