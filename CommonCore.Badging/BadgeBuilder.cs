﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeBuilder" /> <c>class</c> controls the conversion
    /// of <see cref="Badge"/> objects to svg images.
    /// </summary>
    public class BadgeBuilder
    {
        /// <summary>
        /// Gets the default instance of the <see cref="BadgeBuilder"/>.
        /// This instance can and should be used for all general
        /// <see cref="Badge"/> to svg conversions.
        /// </summary>
        public static BadgeBuilder Default { get; } = new BadgeBuilder();

        /// <summary>
        /// Builds the svg image representation of the specified <paramref name="badge"/>.
        /// </summary>
        /// <param name="badge">The badge to be converted to a svg image.</param>
        /// <param name="includeXmlDeclaration">
        /// Indicates whether the xml declaration should be included in the svg content.
        /// </param>
        /// <returns>The svg image representation of the specified <paramref name="badge"/>.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> is <c>null</c>.
        /// </exception>
        public virtual string Build(Badge badge, bool includeXmlDeclaration = true)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));

            Size badgeSize = this.CalBadgeSize(badge);
            var builder = new StringBuilder();

            if (includeXmlDeclaration) { builder.AppendLine(@"<?xml version=""1.0"" encoding=""UTF-8""?>"); }
            builder.AppendLine($@"<svg width=""{badgeSize.Width}"" height=""{badgeSize.Height}"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">");

            if (badge.IsNotEmpty())
            {
                // Gather all the non-empty sections.
                IReadOnlyList<BadgeSection> sections = badge.Sections.Where(section => section.IsNotEmpty()).ToArray();
                // Define gloss gradient, but only if needed by non-empty sections.
                if (sections.Any(section => section.IconStyle.EnableGloss || section.TextStyle.EnableGloss))
                {
                    builder.AppendLine(@"    <defs>");
                    builder.AppendLine(@"        <linearGradient id=""gloss"" x2=""0"" y2=""100%"">");
                    builder.AppendLine(@"            <stop offset=""0"" stop-color=""#bbbbbb"" stop-opacity="".1""/>");
                    builder.AppendLine(@"            <stop offset=""1"" stop-opacity="".1""/>");
                    builder.AppendLine(@"        </linearGradient>");
                    builder.AppendLine(@"    </defs>");
                }

                var point = Point.Empty;
                for (int index = 0; index < sections.Count; index++)
                {
                    var section = sections[index];
                    bool first = index == 0;
                    bool last = index == sections.Count - 1;
                    //Size sectionSize = this.CalSectionSize(badge, section);
                    //// Draw the section background.
                    //this.AppendRoundedRect(builder, point, sectionSize, badge.Roundness, first, last, section.Color.ToHex());
                    //// Apply gloss to the section background.
                    //if (section.EnableColorGloss)
                    //{ this.AppendRoundedRect(builder, point, sectionSize, badge.Roundness, first, last, "url(#gloss)"); }

                    if (section.IconExists)
                    {
                        var areaSize = new Size(this.CalIconPaddedWidth(badge, section), badge.Height);

                        // Draw the section icon area background.
                        this.AppendRoundedRect(builder, point, areaSize, badge.Roundness, first, last && !section.TextExists, BadgeUtils.ToHex(section.IconStyle.BackColor));

                        // Apply gloss to the section icon area background.
                        if (section.IconStyle.EnableGloss)
                        { this.AppendRoundedRect(builder, point, areaSize, badge.Roundness, first, last && !section.TextExists, "url(#gloss)"); }

                        var iconRect = this.CalIconRect(badge, section);

                        // Add shadow image element
                        if (section.IconStyle.EnableShadow)
                        {
                            builder.AppendLine($@"    <image x=""{point.X + iconRect.X}"" y=""{point.Y + iconRect.Y}"" width=""{iconRect.Width}"" height=""{iconRect.Height}"" viewBox=""0 0 {iconRect.Width} {iconRect.Height}"" xlink:href=""data:image/svg+xml;base64,{section.Icon}"" fill=""#010101"" />");
                        }

                        // Add content image element
                        builder.AppendLine($@"    <image x=""{point.X + iconRect.X}"" y=""{point.Y + iconRect.Y}"" width=""{iconRect.Width}"" height=""{iconRect.Height}"" viewBox=""0 0 {iconRect.Width} {iconRect.Height}"" xlink:href=""data:image/svg+xml;base64,{section.Icon}"" />");

                        // Move the draw position to the right.
                        point.Offset(areaSize.Width, 0);
                    }

                    if (section.TextExists)
                    {
                        var areaSize = new Size(this.CalTextPaddedWidth(badge, section), badge.Height);

                        // Draw the section icon area background.
                        this.AppendRoundedRect(builder, point, areaSize, badge.Roundness, first && !section.IconExists, last, BadgeUtils.ToHex(section.TextStyle.BackColor));

                        // Apply gloss to the section icon area background.
                        if (section.TextStyle.EnableGloss)
                        { this.AppendRoundedRect(builder, point, areaSize, badge.Roundness, first && !section.IconExists, last, "url(#gloss)"); }

                        var textRect = this.CalTextRect(badge, section);

                        // Add the shadow text elements.
                        if (section.TextStyle.EnableShadow)
                        { builder.AppendLine($@"    <text x=""{point.X + textRect.X + 1}"" y=""{point.Y + textRect.Y + 1}"" fill=""#010101"" fill-opacity="".3"" font-family=""{section.TextStyle.Font.Name}"" font-size=""{section.TextStyle.FontSize}"">{HttpUtility.HtmlEncode(section.Text)}</text>"); }

                        // Add the content text elements.
                        builder.AppendLine($@"    <text x=""{point.X + textRect.X}"" y=""{point.Y + textRect.Y}"" fill=""{BadgeUtils.ToHex(section.TextStyle.ForeColor)}"" font-family=""{section.TextStyle.Font.Name}"" font-size=""{section.TextStyle.FontSize}"">{HttpUtility.HtmlEncode(section.Text)}</text>");

                        // Move the draw position to the right.
                        point.Offset(areaSize.Width, 0);
                        // width=""{textArea.Width}"" height=""{textArea.Height}""
                        // text-anchor=""middle"" dominant-baseline=""middle"" alignment-baseline=""middle""
                    }
                }
            }

            builder.AppendLine(@"</svg>");
            return builder.Replace("\r\n", "\n").ToString();
        }

        /// <summary>
        /// Calculates the width of the specified <paramref name="badge"/>.
        /// </summary>
        /// <param name="badge">The badge to be measured.</param>
        /// <returns>The width of the specified <paramref name="badge"/>.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> is <c>null</c>.
        /// </exception>
        public virtual int CalBadgeWidth(Badge badge)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));

            return badge.Sections.Sum(section => this.CalSectionWidth(badge, section));
        }

        /// <summary>
        /// Calculates the width of the specified <paramref name="section"/>
        /// within the specified <paramref name="badge"/>.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section to be measured.</param>
        /// <returns>
        /// The width of the specified <paramref name="section"/>
        /// within the specified <paramref name="badge"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual int CalSectionWidth(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return section.IsEmpty() ? 0 : this.CalIconPaddedWidth(badge, section) + this.CalTextPaddedWidth(badge, section);
        }

        /// <summary>
        /// Calculates the width of the specified <paramref name="section"/>
        /// icon, including the total horizontal icon padding.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the icon to be measured.</param>
        /// <returns>
        /// The width of the specified <paramref name="section"/>
        /// icon, including the total horizontal icon padding.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual int CalIconPaddedWidth(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return !section.IconExists ? 0 : (section.IconStyle.Padding * 2) + this.CalIconSize(badge, section).Width;
        }

        /// <summary>
        /// Calculates the width of the specified <paramref name="section"/>
        /// text, including the total horizontal text padding.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the text to be measured.</param>
        /// <returns>
        /// The width of the specified <paramref name="section"/>
        /// text, including the total horizontal text padding.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual int CalTextPaddedWidth(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return !section.TextExists ? 0 : (section.TextStyle.Padding * 2) + (int)Math.Ceiling(section.TextStyle.Font.CalTextSize(section.Text, section.TextStyle.FontSize).Width);
        }

        /// <summary>
        /// Calculates the size of the specified <paramref name="badge"/>.
        /// </summary>
        /// <param name="badge">The badge to be measured.</param>
        /// <returns>The size of the specified <paramref name="badge"/>.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> is <c>null</c>.
        /// </exception>
        public virtual Size CalBadgeSize(Badge badge)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));

            return badge.IsEmpty() ? Size.Empty : new Size(this.CalBadgeWidth(badge), badge.Height);
        }

        /// <summary>
        /// Calculates the size of the specified <paramref name="section"/>
        /// within the specified <paramref name="badge"/>.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section to be measured.</param>
        /// <returns>
        /// The size of the specified <paramref name="section"/>
        /// within the specified <paramref name="badge"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual Size CalSectionSize(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return section.IsEmpty() ? Size.Empty : new Size(this.CalSectionWidth(badge, section), badge.Height);
        }

        /// <summary>
        /// Calculates the size of the specified <paramref name="section"/>
        /// icon, excluding any icon padding.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the icon to be measured.</param>
        /// <returns>
        /// The size of the specified <paramref name="section"/>
        /// icon, excluding any icon padding.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        /// <remarks>
        /// Ideally we would get the embedded size of the icon and scale the width
        /// down based on the padded shrinking of the height, allowing the section
        /// width to expand for horizontally longer rectangles.
        /// However currently we can't get the embedded size, so assume the icon
        /// width and height are equal to the badge height and shrink both equally.
        /// </remarks>
        public virtual Size CalIconSize(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return !section.IconExists ? Size.Empty : new Size(badge.Height - (section.IconStyle.Padding * 2), badge.Height - (section.IconStyle.Padding * 2));
        }

        /// <summary>
        /// Calculates the size of the specified <paramref name="section"/>
        /// text, excluding any text padding.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the text to be measured.</param>
        /// <returns>
        /// The size of the specified <paramref name="section"/>
        /// text, excluding any text padding.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual Size CalTextSize(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            return !section.TextExists ? Size.Empty : Size.Ceiling(section.TextStyle.Font.CalTextSize(section.Text, section.TextStyle.FontSize));
        }

        /// <summary>
        /// Calculate the display rect of the specified <paramref name="section"/>
        /// icon, excluding any icon padding from the size.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the icon to be measured.</param>
        /// <returns>
        /// The display rect of the specified <paramref name="section"/>
        /// icon, excluding any icon padding from the size.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual Rectangle CalIconRect(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            if (!section.IconExists) { return Rectangle.Empty; }
            var size = this.CalIconSize(badge, section);
            var point = new Point(section.IconStyle.Padding, section.IconStyle.Padding);
            return new Rectangle(point, size);
        }

        /// <summary>
        /// Calculate the display rect of the specified <paramref name="section"/>
        /// text, excluding any text padding from the size.
        /// </summary>
        /// <param name="badge">The badge that contains the section.</param>
        /// <param name="section">The section with the text to be measured.</param>
        /// <returns>
        /// The display rect of the specified <paramref name="section"/>
        /// text, excluding any text padding from the size.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="badge"/> or <paramref name="section"/> are <c>null</c>.
        /// </exception>
        public virtual Rectangle CalTextRect(Badge badge, BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(badge, nameof(badge));
            BadgeUtils.ThrowArgNull(section, nameof(section));

            if (!section.TextExists) { return Rectangle.Empty; }
            var size = this.CalTextSize(badge, section);
            var point = new Point(section.TextStyle.Padding, size.Height + 1);
            return new Rectangle(point, size);
        }

        /// <summary>
        /// Appends a svg path element of a rectangle to the specified <paramref name="builder" />.
        /// Optionally, the rectangle can have rounded corners on left and/or right sizes,
        /// based on the specified <paramref name="roundness"/> and the values specified by
        /// <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="builder">The buffer to append to.</param>
        /// <param name="point">The top left position of the rectangle.</param>
        /// <param name="size">The size of the rectangle.</param>
        /// <param name="roundness">
        /// The roundness of the corners of the rectangle.
        /// Specify a value of <c>0</c> to disable round corners.
        /// Negative values are converted to <c>0</c>.
        /// Ignored if <paramref name="left"/> and <paramref name="right"/> are <c>false</c>.
        /// </param>
        /// <param name="left">
        /// Indicates that the left side corners should be rounded.
        /// Ignored if <paramref name="roundness"/> is <c>0</c>.
        /// </param>
        /// <param name="right">
        /// Indicates that the right side corners should be rounded.
        /// Ignored if <paramref name="roundness"/> is <c>0</c>.
        /// </param>
        /// <param name="fill">
        /// The svg-valid value passed to the <c>fill</c> attribute of
        /// the rectangle path element.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="builder"/> or <paramref name="fill"/> are <c>null</c>.
        /// </exception>
        public virtual void AppendRoundedRect(StringBuilder builder, Point point, Size size, int roundness, bool left, bool right, string fill)
        {
            BadgeUtils.ThrowArgNull(builder, nameof(builder));
            BadgeUtils.ThrowArgNull(fill, nameof(fill));

            if (roundness > 0 && left && right)
            {
                Size small = size - new Size(roundness * 2, roundness * 2);
                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X + roundness},{point.Y} h{small.Width} q{roundness},0 {roundness},{roundness} v{small.Height} q0,{roundness} -{roundness},{roundness} h-{small.Width} q-{roundness},0 -{roundness},-{roundness} v-{small.Height} q0,-{roundness} {roundness},-{roundness} z"" /> ");
            }
            else if (roundness > 0 && left)
            {
                Size small = size - new Size(roundness, roundness * 2);
                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X + size.Width},{point.Y} h-{small.Width} q-{roundness},0 -{roundness},{roundness} v{small.Height} q0,{roundness} {roundness},{roundness} h{small.Width} z"" /> ");
            }
            else if (roundness > 0 && right)
            {
                Size small = size - new Size(roundness, roundness * 2);
                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X},{point.Y} h{small.Width} q{roundness},0 {roundness},{roundness} v{small.Height} q0,{roundness} -{roundness},{roundness} h-{small.Width} z"" /> ");
            }
            else
            {
                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X},{point.Y} h{size.Width} v{size.Height} h-{size.Width} z"" /> ");
            }
        }
    }
}
