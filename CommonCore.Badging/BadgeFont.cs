﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeFont" /> <c>class</c> represents the font of a badge section text.
    /// </summary>
    public class BadgeFont
    {
        /// <summary>
        /// Stores the object used for locking against concurrent use of the <see cref="Measurer"/> property.
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// Gets the default value for <see cref="BadgeFont.Name"/>.
        /// The default value is 'DejaVu Sans,Verdana,Geneva,sans-serif'.
        /// </summary>
        public static string DefaultFontName { get; } = "DejaVu Sans,Verdana,Geneva,sans-serif";

        /// <summary>
        /// Gets the default value for <see cref="BadgeFont.Size"/>.
        /// The default value is '11'.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public static int DefaultFontSize { get; } = 11;

        /// <summary>
        /// Gets the default value for <see cref="BadgeFont.Color"/>.
        /// The default value is 'Color.White'.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public static Color DefaultFontColor { get; } = Color.White;

        /// <summary>
        /// Gets the default value for <see cref="BadgeFont.Fixed"/>.
        /// The default value is 'false'.
        /// </summary>
        public static bool DefaultFontFixed { get; } = false;

        /// <summary>
        /// Initializes a <see cref="BadgeFont" /> instance with default values.
        /// </summary>
        public BadgeFont() { }

        /// <summary>
        /// Initializes a <see cref="BadgeFont" /> instance with specified values.
        /// </summary>
        /// <param name="fontName">The name of the font.</param>
        /// <param name="fontFixed">Indicates the fixed-width quality of the font.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="fontName"/> is <c>null</c>.</exception>
        public BadgeFont(string fontName, bool fontFixed = false)
        {
            this.Name = fontName ?? throw new ArgumentNullException(nameof(fontName));
            this.Fixed = fontFixed;
        }

        /// <summary>
        /// Initializes a <see cref="BadgeFont" /> instance with specified values.
        /// </summary>
        /// <param name="fontName">The name of the font.</param>
        /// <param name="fontSize">The size of the font.</param>
        /// <param name="fontFixed">Indicates the fixed-width quality of the font.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="fontName"/> is <c>null</c>.</exception>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public BadgeFont(string fontName, int fontSize, bool fontFixed = false)
        {
            this.Name = fontName ?? throw new ArgumentNullException(nameof(fontName));
            this.Size = fontSize;
            this.Fixed = fontFixed;
        }

        /// <summary>
        /// Initializes a <see cref="BadgeFont" /> instance with specified values.
        /// </summary>
        /// <param name="fontName">The name of the font.</param>
        /// <param name="fontSize">The size of the font.</param>
        /// <param name="fontColor">The color of the font.</param>
        /// <param name="fontFixed">Indicates the fixed-width quality of the font.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="fontName"/> is <c>null</c>.</exception>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public BadgeFont(string fontName, int fontSize, Color fontColor, bool fontFixed = false)
        {
            this.Name = fontName ?? throw new ArgumentNullException(nameof(fontName));
            this.Size = fontSize;
            this.Color = fontColor;
            this.Fixed = fontFixed;
        }

        /// <summary>
        /// Gets the name of the font.
        /// </summary>
        public string Name { get; set; } = DefaultFontName;

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeStyle.FontSize"/> of <see cref="BadgeSection.TextStyle"/> instead.
        /// Gets the size of the font.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeSection.TextStyle.FontSize instead.", false)]
        public int Size { get; set; } = DefaultFontSize;

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeStyle.ForeColor"/> of <see cref="BadgeSection.TextStyle"/> instead.
        /// Gets the color of the font.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeSection.TextStyle.ForeColor instead.", false)]
        public Color Color { get; set; } = DefaultFontColor;

        /// <summary>
        /// Gets a value indicating if the font is fixed-width, ie monospaced.
        /// </summary>
        public bool Fixed { get; set; } = DefaultFontFixed;

        /// <summary>
        /// Calculate the size of the specified <paramref name="text"/> according to the font.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <param name="fontSize">The font size of the text.</param>
        /// <returns>The size of the specified <paramref name="text"/> according to the font.</returns>
        public SizeF CalTextSize(string text, float fontSize = 11)
        {
            var fontName = this.Name?.Split(',')[0] ?? "DejaVu Sans";
            using var font = new Font(fontName, fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
            lock (BadgeFont.Lock) { return Measurer.MeasureString(text, font, 0, StringFormat.GenericTypographic); }
        }

        /// <summary>
        /// Calculate the width of the specified <paramref name="text"/> according to the font.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <param name="fontSize">The font size of the text.</param>
        /// <returns>The width of the specified <paramref name="text"/> according to the font.</returns>
        public float CalTextWidth(string text, float fontSize = 11) => this.CalTextSize(text, fontSize).Width;

        /// <summary>
        /// Gets the <see cref="Graphics"/> objected used to measure text.
        /// </summary>
        private static Graphics Measurer { get; } = Graphics.FromImage(new Bitmap(1, 1));
    }
}
