﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeStyle" /> <c>class</c> represents the styles used to
    /// renderer an area of a <see cref="BadgeSection"/>.
    /// </summary>
    public class BadgeStyle
    {
        /// <summary>
        /// Returns a new <see cref="BadgeStyle"/> instance with default values set.
        /// <see cref="ForeColor"/> to <see cref="BadgeColors.Default"/>;
        /// <see cref="BackColor"/> to <see cref="BadgeColors.White"/>;
        /// <see cref="Padding"/> to <c>4</c>;
        /// <see cref="EnableGloss"/> to <c>true</c>;
        /// <see cref="EnableShadow"/> to <c>true</c>.
        /// </summary>
        /// <returns>A new <see cref="BadgeStyle"/> instance with default values set.</returns>
        public static BadgeStyle CreateDefault() => new BadgeStyle(BadgeColors.Default, BadgeColors.White, 4, true, true);

        /// <summary>
        /// Returns a new <see cref="BadgeStyle"/> instance with default values set.
        /// <see cref="ForeColor"/> to the specified <paramref name="backColor"/>;
        /// <see cref="BackColor"/> to <see cref="BadgeColors.White"/>;
        /// <see cref="Padding"/> to <c>4</c>;
        /// <see cref="EnableGloss"/> to <c>true</c>;
        /// <see cref="EnableShadow"/> to <c>true</c>.
        /// </summary>
        /// <param name="backColor">The background color of the style.</param>
        /// <returns>A new <see cref="BadgeStyle"/> instance with default values set.</returns>
        public static BadgeStyle CreateDefault(Color backColor) => new BadgeStyle(backColor, BadgeColors.White, 4, true, true);

        /// <summary>
        /// Initializes a <see cref="BadgeStyle" /> instance.
        /// </summary>
        public BadgeStyle() { }

        /// <summary>
        /// Initializes a <see cref="BadgeStyle" /> instance.
        /// </summary>
        /// <param name="backColor">The background color of the style.</param>
        /// <param name="foreColor">The foreground color of the style.</param>
        /// <param name="padding">The padding of the style.</param>
        /// <param name="enableGloss">Whether a linear gradient gloss effect is applied to the style <see cref="BackColor"/>.</param>
        /// <param name="enableShadow">Whether a shadow effect is applied to the style foreground content.</param>
        public BadgeStyle(Color backColor, Color foreColor, int padding, bool enableGloss, bool enableShadow)
        {
            this.BackColor = backColor;
            this.ForeColor = foreColor;
            this.Padding = padding;
            this.EnableGloss = enableGloss;
            this.EnableShadow = enableShadow;
        }

        /// <summary>
        /// Gets or sets the background color of the style.
        /// </summary>
        public Color BackColor { get; set; } = BadgeColors.Default;

        /// <summary>
        /// Gets or sets the foreground color of the style.
        /// </summary>
        public Color ForeColor { get; set; } = BadgeColors.White;

        /// <summary>
        /// Gets or sets the font size of text style.
        /// </summary>
        public int FontSize { get; set; } = 11;

        /// <summary>
        /// Gets or sets the font of the text style.
        /// </summary>
        public BadgeFont Font { get; set; } = new BadgeFont();

        /// <summary>
        /// Gets or sets the padding of the style.
        /// </summary>
        public int Padding { get; set; } = 0;

        /// <summary>
        /// Gets or sets whether a linear gradient gloss effect is applied to the style <see cref="BackColor"/>.
        /// </summary>
        public bool EnableGloss { get; set; } = false;

        /// <summary>
        /// Gets or sets whether a shadow effect is applied to the style foreground content.
        /// </summary>
        public bool EnableShadow { get; set; } = false;
    }
}
