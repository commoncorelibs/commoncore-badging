﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Net;
using System.Text;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeUtils" /> <c>static</c> <c>extension</c> <c>class</c> provides miscellaneous
    /// extension methods used by the <see cref="CommonCore.Badging"/> library.
    /// </summary>
    public static class BadgeUtils
    {
        /// <summary>
        /// Returns the specified <paramref name="color"/> converted to its six-digit
        /// hex (#000000) <see cref="string"/> representation.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>
        /// The specified <paramref name="color"/> converted to its six-digit
        /// hex (#000000) <see cref="string"/> representation.
        /// </returns>
        public static string ToHex(Color color) => $"#{color.R:X2}{color.G:X2}{color.B:X2}";

        /// <summary>
        /// Returns the specified <paramref name="color"/> converted to its three-part
        /// comma-separated rgb (255,255,255) <see cref="string"/> representation.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>
        /// The specified <paramref name="color"/> converted to its three-part
        /// comma-separated rgb (255,255,255) <see cref="string"/> representation.
        /// </returns>
        public static string ToRgb(Color color) => $"{color.R},{color.G},{color.B}";

        /// <summary>
        /// Returns the specified <paramref name="value"/> converted to its
        /// base64 encoded <see cref="string"/> representation.
        /// </summary>
        /// <param name="value">The value to encode.</param>
        /// <returns>
        /// The specified <paramref name="value"/> converted to its
        /// base64 encoded <see cref="string"/> representation.
        /// </returns>
        public static string ToBase64(string value) => Convert.ToBase64String(Encoding.UTF8.GetBytes(value));

        /// <summary>
        /// Downloads the specified <paramref name="uri"/> as a <see cref="string"/>.
        /// </summary>
        /// <param name="uri">The uri to download.</param>
        /// <returns>The specified <paramref name="uri"/> as a <see cref="string"/>.</returns>
        [ExcludeFromCodeCoverage]
        public static string DownloadString(Uri uri) { using var client = new WebClient(); return client.DownloadString(uri); }

        /// <summary>
        /// Downloads the specified <paramref name="uri"/> as a <see cref="string"/> and encoded as base64.
        /// </summary>
        /// <param name="uri">The uri to download.</param>
        /// <returns>The specified <paramref name="uri"/> as a <see cref="string"/> and encoded as base64.</returns>
        [ExcludeFromCodeCoverage]
        public static string DownloadBase64(Uri uri) => BadgeUtils.ToBase64(BadgeUtils.DownloadString(uri));

        /// <summary>
        /// Throws <see cref="ArgumentNullException"/> if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">Type of the value.</typeparam>
        /// <param name="value">The value to check.</param>
        /// <param name="name">The name of the argument.</param>
        public static void ThrowArgNull<T>(T value, string name) where T : class
        { if (value is null) { throw new ArgumentNullException(name); } }
    }
}
