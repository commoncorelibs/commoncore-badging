﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#if DEBUG
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;

namespace CommonCore.Badging.Compiling
{
    /// <summary>
    /// The <see cref="BadgeIndexHtmlGenerator" /> <c>static</c> <c>meta</c> <c>class</c> is used to
    /// generate a html file with examples of badges.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class BadgeIndexHtmlGenerator
    {
        /// <summary>
        /// Run the generator and return the output and count of items.
        /// </summary>
        /// <returns></returns>
        public static (int Count, StringBuilder Builder) Run()
        {
            var builder = new StringBuilder();
            int count = Write(builder);
            return (count, builder);
        }

        /// <summary>
        /// Run the generator and write the output to the specified <paramref name="output"/> file path
        /// or the console if <c>null</c> is specified.
        /// </summary>
        /// <param name="output">The file path to write the output or <c>null</c> to use stdout.</param>
        public static void Run(string output)
        {
            var builder = new StringBuilder();

            int count = Write(builder);
            if (string.IsNullOrEmpty(output)) { Console.WriteLine(builder.ToString()); }
            else { File.WriteAllText(Path.GetFullPath(output), builder.ToString()); }

            Console.WriteLine();
            Console.WriteLine($"Examples: {count}");
        }

        private static int Write(StringBuilder builder)
        {
            var dict = GenDict();
            int count = dict.Sum(pair => pair.Value.Count);

            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html>");
            builder.AppendLine("    <head>");
            builder.AppendLine("    </head>");
            builder.AppendLine("    <body style=\"background:#999999;\">");

            builder.AppendLine();
            builder.AppendLine("    <h1>Example Badges</h1>");
            builder.AppendLine();
            builder.AppendLine($"    <p>Total Count: {count}</p>");
            foreach (var (title, badges) in dict)
            {
                builder.AppendLine();
                builder.AppendLine("    <article>");
                builder.AppendLine($"        <h2>Badge {title}</h2>");
                builder.AppendLine();
                builder.AppendLine($"        <p>Count: {badges.Count}</p>");
                builder.AppendLine();
                builder.AppendLine("        <ol>");
                foreach (var badge in badges)
                { builder.AppendLine($"            <li><img src=\"data:image/svg+xml;base64,{badge.ToBase64()}\" /></li>"); }
                builder.AppendLine("        </ol>");
                builder.AppendLine("    </article>");
            }

            builder.AppendLine("    </body>");
            builder.AppendLine("</html>");

            return count;
        }

        private static IDictionary<string, IList<Badge>> GenDict()
        {
            var dict = new Dictionary<string, IList<Badge>>();
            IList<Badge> badges;

            badges = new List<Badge>();
            foreach (var (key, font) in BadgeFonts.Dict)
            {
                var badge = new Badge(BadgeIcons.Info, "font", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(BadgeColors.BBlue));
                badge.Sections[1].TextStyle.Font = font;
                badges.Add(badge);
            }
            dict.Add("Fonts", badges);

            badges = new List<Badge>();
            foreach (var (key, image) in BadgeIcons.Dict)
            { badges.Add(new Badge(image, "icon", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(BadgeColors.BBlue))); }
            dict.Add("Icons", badges);

            badges = new List<Badge>();
            foreach (var (key, color) in BadgeColors.Dict)
            {
                badges.Add(new Badge(BadgeIcons.Palette, "color", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(color)));
            }
            dict.Add("Colors", badges);

            return dict;
        }
    }
}
#endif
