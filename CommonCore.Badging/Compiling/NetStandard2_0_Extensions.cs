﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#if DEBUG
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#if NETSTANDARD2_0
namespace CommonCore.Badging.Compiling
{
    internal static class NetStandard2_0_Extensions
    {
        /// <summary>
        /// Deconstruction extension method for <see cref="KeyValuePair{TKey, TValue}"/> objects.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="key">The deconstructed key value.</param>
        /// <param name="value">The deconstructed value value.</param>
        [ExcludeFromCodeCoverage]
        public static void Deconstruct<TKey, TValue>(this System.Collections.Generic.KeyValuePair<TKey, TValue> self, out TKey key, out TValue value)
        {
            key = self.Key;
            value = self.Value;
        }
    }
}
#endif
#endif
