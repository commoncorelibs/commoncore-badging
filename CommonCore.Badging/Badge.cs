﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CommonCore.Badging
{
    // Icon Sources:
    // https://github.com/badgen/badgen-icons
    // https://simpleicons.org/
    // https://github.com/simple-icons/simple-icons
    // https://feathericons.com/
    // https://github.com/feathericons/feather
    // https://materialdesignicons.com/
    // This library might be useful for auto-optimizing input svg images.
    // https://github.com/dabutvin/SvgClean
    /// <summary>
    /// The <see cref="Badge" /> <c>class</c> represents a collection of
    /// <see cref="BadgeSection"/> objects.
    /// </summary>
    public class Badge : IEnumerable<BadgeSection>
    {
        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with no sections.
        /// </summary>
        public Badge() { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one icon-only section.
        /// The default style will be used for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        public Badge(BadgeIcon icon) : this(new BadgeSection(icon)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one icon-only section.
        /// The specified <paramref name="style"/> will be used for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="style">The style of the icon.</param>
        public Badge(BadgeIcon icon, BadgeStyle style) : this(new BadgeSection(icon, style)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one icon-only section.
        /// The default style with the specified <paramref name="backColor"/> will be used for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="backColor">The background color of the style of the icon.</param>
        public Badge(BadgeIcon icon, Color backColor) : this(new BadgeSection(icon, backColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one text-only section.
        /// The default style will be used for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        public Badge(string text) : this(new BadgeSection(text)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one text-only section.
        /// The specified <paramref name="style"/> will be used for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the icon.</param>
        public Badge(string text, BadgeStyle style) : this(new BadgeSection(text, style)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one text-only section.
        /// The default style with the specified <paramref name="backColor"/> will be used for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the style of the text.</param>
        public Badge(string text, Color backColor) : this(new BadgeSection(text, backColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one mixed icon and text section.
        /// The default style will be used for the specified <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        public Badge(BadgeIcon icon, string text) : this(new BadgeSection(icon, text)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="style"/> for the specified
        /// <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the icon and text.</param>
        public Badge(BadgeIcon icon, string text, BadgeStyle style)
            : this(new BadgeSection(icon, text, style)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="backColor"/> for the specified
        /// <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the style of the icon and text.</param>
        public Badge(BadgeIcon icon, string text, Color backColor)
            : this(new BadgeSection(icon, text, backColor)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="iconStyle"/> for the specified <paramref name="icon"/>
        /// and using the specified <paramref name="textStyle"/> for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconStyle">The style of the icon.</param>
        /// <param name="textStyle">The style of the text.</param>
        public Badge(BadgeIcon icon, string text, BadgeStyle iconStyle, BadgeStyle textStyle)
            : this(new BadgeSection(icon, text, iconStyle, textStyle)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="iconBackColor"/> for the specified <paramref name="icon"/>
        /// and using the default style with the specified <paramref name="textBackColor"/> for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconBackColor">The background color of the style of the icon.</param>
        /// <param name="textBackColor">The background color of the style of the text.</param>
        public Badge(BadgeIcon icon, string text, Color iconBackColor, Color textBackColor)
            : this(new BadgeSection(icon, text, iconBackColor, textBackColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with two sections;
        /// the text-only key section and the text-only value section.
        /// The default style will be used for the specified <paramref name="keyText"/>.
        /// </summary>
        /// <param name="keyText">The text of the key section.</param>
        /// <param name="valueText">The text of the value section.</param>
        /// <param name="valueTextStyle">The style of the text of the value section.</param>
        public Badge(string keyText, string valueText, BadgeStyle valueTextStyle)
            : this(new BadgeSection(keyText), new BadgeSection(valueText, valueTextStyle)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with two sections;
        /// the text-only key section and the text-only value section.
        /// The default style will be used for the specified <paramref name="keyText"/>
        /// and the default style with the specified <paramref name="valueBackColor"/>
        /// will be used for the specified <paramref name="valueText"/>.
        /// </summary>
        /// <param name="keyText">The text of the key section.</param>
        /// <param name="valueText">The text of the value section.</param>
        /// <param name="valueBackColor">The background color of the style of the text of the value section.</param>
        public Badge(string keyText, string valueText, Color valueBackColor)
            : this(new BadgeSection(keyText), new BadgeSection(valueText, valueBackColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with two sections;
        /// the mixed icon and text key section and the text-only value section.
        /// The default style will be used for the specified <paramref name="keyIcon"/> and <paramref name="keyText"/>.
        /// </summary>
        /// <param name="keyIcon">The icon of the key section.</param>
        /// <param name="keyText">The text of the key section.</param>
        /// <param name="valueText">The text of the value section.</param>
        /// <param name="valueTextStyle">The style of the text of the value section.</param>
        public Badge(BadgeIcon keyIcon, string keyText, string valueText, BadgeStyle valueTextStyle)
            : this(new BadgeSection(keyIcon, keyText), new BadgeSection(valueText, valueTextStyle)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with two sections;
        /// the mixed icon and text key section and the text-only value section.
        /// The default style will be used for the specified <paramref name="keyIcon"/> and <paramref name="keyText"/>.
        /// and the default style with the specified <paramref name="valueBackColor"/>
        /// will be used for the specified <paramref name="valueText"/>.
        /// </summary>
        /// <param name="keyIcon">The icon of the key section.</param>
        /// <param name="keyText">The text of the key section.</param>
        /// <param name="valueText">The text of the value section.</param>
        /// <param name="valueBackColor">The style of the text of the value section.</param>
        public Badge(BadgeIcon keyIcon, string keyText, string valueText, Color valueBackColor)
            : this(new BadgeSection(keyIcon, keyText), new BadgeSection(valueText, valueBackColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one or more sections.
        /// </summary>
        /// <param name="section">First section of the badge.</param>
        /// <param name="sections">Additional sections of the badge.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="section"/> is <c>null</c>.</exception>
        public Badge(BadgeSection section, params BadgeSection[] sections)
        {
            if (section == null) { throw new ArgumentNullException(nameof(section)); }

            this.Sections.Add(section);
            if (sections != null && sections.Length > 0)
            { this.Sections.AddRange(sections); }
        }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with zero or more sections.
        /// </summary>
        /// <param name="sections">Sections of the badge.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="sections"/> is <c>null</c>.</exception>
        public Badge(IEnumerable<BadgeSection> sections)
        {
            if (sections == null) { throw new ArgumentNullException(nameof(sections)); }
            this.Sections.AddRange(sections);
        }

        /// <summary>
        /// Gets the height of the badge. The value is hardcoded to 20.
        /// </summary>
        public int Height { get; } = 20;

        /// <summary>
        /// Gets or sets the amount of roundness applied to the badge corners.
        /// </summary>
        public int Roundness { get; set; } = 3;

        /// <summary>
        /// Get the list of <see cref="BadgeSection"/> objects that compose this badge.
        /// </summary>
        public List<BadgeSection> Sections { get; } = new List<BadgeSection>();

        /// <summary>
        /// Gets the count of sections.
        /// </summary>
        public int Count => this.Sections.Count;

        /// <summary>
        /// Gets or sets the <see cref="BadgeSection"/> at the specified <paramref name="index"/>.
        /// </summary>
        /// <param name="index">The index of the section.</param>
        /// <returns>The <see cref="BadgeSection"/> at the specified <paramref name="index"/>.</returns>
        public BadgeSection this[int index] { get => this.Sections[index]; set => this.Sections[index] = value; }

        /// <summary>
        /// Determines if this instance contains no sections or all sections are empty.
        /// </summary>
        /// <returns><c>true</c> if section count is <c>0</c> or all sections are <c>empty</c>; otherwise <c>false</c>.</returns>
        public bool IsEmpty()
        {
            if (this.Sections.Count == 0) { return true; }
            foreach (var section in this.Sections) { if (section.IsNotEmpty()) { return false; } }
            return true;
        }

        /// <summary>
        /// Determines if this instance contains any non-empty sections.
        /// </summary>
        /// <returns><c>true</c> if section count greater than <c>0</c> and at least one section is not <c>empty</c>; otherwise <c>false</c>.</returns>
        public bool IsNotEmpty()
        {
            if (this.Sections.Count == 0) { return false; }
            foreach (var section in this.Sections) { if (section.IsNotEmpty()) { return true; } }
            return false;
        }

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon)
            => this.Add(new BadgeSection(icon));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="style">The style of the icon.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, BadgeStyle style)
            => this.Add(new BadgeSection(icon, style));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="backColor">The background color of the style.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, Color backColor)
            => this.Add(new BadgeSection(icon, backColor));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(string text)
            => this.Add(new BadgeSection(text));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the text.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(string text, BadgeStyle style)
            => this.Add(new BadgeSection(text, style));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the style.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(string text, Color backColor)
            => this.Add(new BadgeSection(text, backColor));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, string text)
            => this.Add(new BadgeSection(icon, text));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the icon and text.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, string text, BadgeStyle style)
            => this.Add(new BadgeSection(icon, text, style));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the icon and text styles.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, string text, Color backColor)
            => this.Add(new BadgeSection(icon, text, backColor));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconStyle">The style of the icon.</param>
        /// <param name="textStyle">The style of the text.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, string text, BadgeStyle iconStyle, BadgeStyle textStyle)
            => this.Add(new BadgeSection(icon, text, iconStyle, textStyle));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconBackColor">The background color of the icon style.</param>
        /// <param name="textBackColor">The background color of the text style.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeIcon icon, string text, Color iconBackColor, Color textBackColor)
            => this.Add(new BadgeSection(icon, text, iconBackColor, textBackColor));

        /// <summary>
        /// Adds a section to the badge.
        /// </summary>
        /// <param name="section">The section to be added.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(BadgeSection section)
        {
            BadgeUtils.ThrowArgNull(section, nameof(section));
            this.Sections.Add(section);
            return this;
        }

        /// <summary>
        /// Adds a collection of sections to the badge.
        /// </summary>
        /// <param name="sections">The collection of sections to be added.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(params BadgeSection[] sections)
        {
            BadgeUtils.ThrowArgNull(sections, nameof(sections));
            this.Sections.AddRange(sections.Where(section => section != null));
            return this;
        }

        /// <summary>
        /// Adds a collection of sections to the badge.
        /// </summary>
        /// <param name="sections">The collection of sections to be added.</param>
        /// <returns>The badge instance in a fluent manner.</returns>
        public Badge Add(IEnumerable<BadgeSection> sections)
        {
            BadgeUtils.ThrowArgNull(sections, nameof(sections));
            this.Sections.AddRange(sections.Where(section => section != null));
            return this;
        }

        /// <inheritdoc cref="ToSvg()"/>
        public override string ToString() => this.ToSvg();

        /// <inheritdoc cref="ToSvg(bool)"/>
        public string ToString(bool includeXmlDeclaration) => this.ToSvg(includeXmlDeclaration);

        /// <summary>
        /// Returns the <see cref="Badge"/> converted to the <paramref name="format"/> specified
        /// <see cref="string"/> representation, optionally including the xml declaration.
        /// </summary>
        /// <param name="format">The format to convert to.</param>
        /// <param name="includeXmlDeclaration">
        /// Indicates whether the xml declaration should be included in the svg content.
        /// </param>
        /// <returns>
        /// The <see cref="Badge"/> converted to the <paramref name="format"/> specified
        /// <see cref="string"/> representation, optionally including the xml declaration.
        /// </returns>
        public string ToString(EBadgeFormat format, bool includeXmlDeclaration = true)
        {
            switch (format)
            {
                case EBadgeFormat.Base64: { return this.ToBase64(includeXmlDeclaration); }
                case EBadgeFormat.Svg:
                default: { return this.ToString(includeXmlDeclaration); }
            }
        }

        /// <summary>
        /// Returns the <see cref="Badge"/> converted to its base64 <see cref="string"/> representation,
        /// including the xml declaration.
        /// </summary>
        /// <returns>
        /// The <see cref="Badge"/> converted to its base64 <see cref="string"/> representation,
        /// including the xml declaration.
        /// </returns>
        public string ToBase64() => this.ToBase64(true);

        /// <summary>
        /// Returns the <see cref="Badge"/> converted to its base64 <see cref="string"/> representation,
        /// optionally including the xml declaration.
        /// </summary>
        /// <param name="includeXmlDeclaration">
        /// Indicates whether the xml declaration should be included in the svg content.
        /// </param>
        /// <returns>
        /// The <see cref="Badge"/> converted to its base64 <see cref="string"/> representation,
        /// optionally including the xml declaration.
        /// </returns>
        public string ToBase64(bool includeXmlDeclaration) => Convert.ToBase64String(Encoding.UTF8.GetBytes(this.ToString(includeXmlDeclaration)));

        /// <summary>
        /// Returns the <see cref="Badge"/> converted to its svg <see cref="string"/> representation,
        /// including the xml declaration.
        /// </summary>
        /// <returns>
        /// The <see cref="Badge"/> converted to its svg <see cref="string"/> representation,
        /// including the xml declaration.
        /// </returns>
        public string ToSvg() => this.ToSvg(true);

        /// <summary>
        /// Returns the <see cref="Badge"/> converted to its svg <see cref="string"/> representation,
        /// optionally including the xml declaration.
        /// </summary>
        /// <param name="includeXmlDeclaration">
        /// Indicates whether the xml declaration should be included in the svg content.
        /// </param>
        /// <returns>
        /// The <see cref="Badge"/> converted to its svg <see cref="string"/> representation,
        /// optionally including the xml declaration.
        /// </returns>
        public string ToSvg(bool includeXmlDeclaration) => BadgeBuilder.Default.Build(this, includeXmlDeclaration);

        #region Implement IEnumerable<BadgeSection>

        /// <inheritdoc cref="IEnumerable{T}" />
        public IEnumerator<BadgeSection> GetEnumerator() => ((IEnumerable<BadgeSection>)this.Sections).GetEnumerator();

        /// <inheritdoc cref="IEnumerable" />
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)this.Sections).GetEnumerator();

        #endregion

        #region Deprecated Members

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with one section.
        /// </summary>
        /// <param name="text">The text of the badge.</param>
        /// <param name="color">The color of the badge.</param>
        /// <param name="icon">The base64 encoded svg icon of the badge.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public Badge(string text, Color color, string icon = null)
            : this(new BadgeSection(text, color, icon)) { }

        ///// <summary>
        ///// Initializes a <see cref="Badge" /> instance with two sections.
        ///// The key section will use the default badge color.
        ///// </summary>
        ///// <param name="keyText">The text of the key section.</param>
        ///// <param name="valueText">The text of the value section.</param>
        ///// <param name="valueColor">The color of the value section.</param>
        //[ExcludeFromCodeCoverage]
        //[Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        //public Badge(string keyText, string valueText, Color valueColor)
        //    : this(new BadgeSection(keyText, BadgeColors.Default), new BadgeSection(valueText, valueColor)) { }

        /// <summary>
        /// Initializes a <see cref="Badge" /> instance with two sections.
        /// The key section will use the default badge color.
        /// </summary>
        /// <param name="keyText">The text of the key section.</param>
        /// <param name="keyIcon">The base64 encoded svg icon of the key section.</param>
        /// <param name="valueText">The text of the value section.</param>
        /// <param name="valueColor">The color of the value section.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public Badge(string keyText, string keyIcon, string valueText, Color valueColor)
            : this(new BadgeSection(keyText, BadgeColors.Default, keyIcon), new BadgeSection(valueText, valueColor)) { }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder.Default"/>
        /// <see cref="BadgeBuilder.CalBadgeSize(Badge)"/> instead.
        /// Calculate the combined width of all contained sections.
        /// </summary>
        /// <returns></returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder.Default.CalBadgeWidth() instead.", false)]
        public int CalWidth() => this.Sections.Sum(section => section.CalWidth());

        #endregion
    }
}
