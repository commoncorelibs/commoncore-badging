﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Drawing;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeIcon" /> <c>record</c> represents a based64 encoded svg image
    /// with optional title, width, and height meta data.
    /// </summary>
    public readonly struct BadgeIcon : IEquatable<BadgeIcon>
    {
        /// <summary>
        /// Gets the <see cref="BadgeIcon"/> instance representing an empty icon.
        /// </summary>
        public static BadgeIcon None { get; } = new BadgeIcon();

        /// <summary>
        /// Initializes a <see cref="BadgeIcon" /> instance.
        /// </summary>
        /// <param name="title">The title of the icon.</param>
        /// <param name="width">The width of the icon.</param>
        /// <param name="height">The height of the icon.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="title"/> is <c>null</c>.
        /// </exception>
        private BadgeIcon(string title, int width, int height)
        {
            BadgeUtils.ThrowArgNull(title, nameof(title));
            this.Title = title;
            this.Width = width;
            this.Height = height;
            this.Content = null;
        }

        /// <summary>
        /// Initializes a <see cref="BadgeIcon" /> instance.
        /// </summary>
        /// <param name="title">The title of the icon.</param>
        /// <param name="width">The width of the icon.</param>
        /// <param name="height">The height of the icon.</param>
        /// <param name="content">The base64 encoded svg image content of the icon.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="title"/> or <paramref name="content"/> are <c>null</c>.
        /// </exception>
        public BadgeIcon(string title, int width, int height, string content)
            : this(title, width, height)
        {
            BadgeUtils.ThrowArgNull(content, nameof(content));
            this.Content = content;
        }

        /// <summary>
        /// Initializes a <see cref="BadgeIcon" /> instance.
        /// Content will be set by downloading the specified <paramref name="uri"/> then
        /// base64 encoding the result.
        /// </summary>
        /// <param name="title">The title of the icon.</param>
        /// <param name="width">The width of the icon.</param>
        /// <param name="height">The height of the icon.</param>
        /// <param name="uri">
        /// The uri of the svg image that will be downloaded and encoded as base64 for the content of the icon.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="title"/> or <paramref name="uri"/> are <c>null</c>.
        /// </exception>
        public BadgeIcon(string title, int width, int height, Uri uri)
            : this(title, width, height)
        {
            BadgeUtils.ThrowArgNull(uri, nameof(uri));
            this.Content = BadgeUtils.DownloadBase64(uri);
        }

        /// <summary>
        /// Initializes a <see cref="BadgeIcon" /> instance.
        /// The <see cref="Title"/> will be set to <c>null</c>;
        /// <see cref="Width"/> and <see cref="Height"/> will be set to <c>20</c>.
        /// </summary>
        /// <param name="content">The base64 encoded svg image content of the icon.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="content"/> is <c>null</c>.
        /// </exception>
        public BadgeIcon(string content)
        {
            BadgeUtils.ThrowArgNull(content, nameof(content));
            this.Title = null;
            this.Width = 20;
            this.Height = 20;
            this.Content = content;
        }

        /// <summary>
        /// Initializes a <see cref="BadgeIcon" /> instance.
        /// Content will be set by downloading the specified <paramref name="uri"/> then
        /// base64 encoding the result.
        /// The <see cref="Title"/> will be set to <c>null</c>;
        /// <see cref="Width"/> and <see cref="Height"/> will be set to <c>20</c>.
        /// </summary>
        /// <param name="uri">
        /// The uri of the svg image that will be downloaded and encoded as base64 for the content of the icon.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="uri"/> is <c>null</c>.
        /// </exception>
        public BadgeIcon(Uri uri)
        {
            BadgeUtils.ThrowArgNull(uri, nameof(uri));
            this.Title = null;
            this.Width = 20;
            this.Height = 20;
            this.Content = BadgeUtils.DownloadBase64(uri);
        }

        /// <summary>
        /// Gets the title of the icon.
        /// </summary>
        public readonly string Title { get; }

        /// <summary>
        /// Gets the width of the icon.
        /// </summary>
        public readonly int Width { get; }

        /// <summary>
        /// Gets the height of the icon.
        /// </summary>
        public readonly int Height { get; }

        /// <summary>
        /// Gets the base64 encoded svg image content of the icon.
        /// </summary>
        public readonly string Content { get; }

        /// <summary>
        /// Gets the size of the icon.
        /// </summary>
        public readonly Size Size => new Size(this.Width, this.Height);

        /// <summary>
        /// Returns a value indicating if the icon content exists.
        /// Returns <c>true</c> if <see cref="Content"/> is not <c>null</c> or <c>empty</c>; otherwise <c>false</c>.
        /// </summary>
        /// <returns><c>true</c> if <see cref="Content"/> is not <c>null</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public readonly bool ContentExists => !string.IsNullOrEmpty(this.Content);

        /// <summary>
        /// Returns a hash code for this <see cref="BadgeIcon"/>.
        /// </summary>
        /// <returns>A hash code for this <see cref="BadgeIcon"/>.</returns>
        public readonly override int GetHashCode() => HashCode.Combine(this.Title, this.Width, this.Height, this.Content);

        /// <summary>
        /// Determines if the specified <paramref name="obj"/> is a <see cref="BadgeIcon" /> and has the same width and height as this <see cref="BadgeIcon" />.
        /// </summary>
        /// <param name="obj">The object to test.</param>
        /// <returns>
        /// <c>true</c> if the specified <paramref name="obj"/> is a <see cref="BadgeIcon" /> and has the same width and height as this <see cref="BadgeIcon" />; otherwise, false.
        /// </returns>
        public readonly override bool Equals(object obj) => obj is BadgeIcon icon && this.Equals(icon);

        /// <summary>
        /// Determines if the specified <paramref name="other"/> <see cref="BadgeIcon" /> has the same width and height as this <see cref="BadgeIcon" />.
        /// </summary>
        /// <param name="other">The icon to test.</param>
        /// <returns>
        /// <c>true</c> if the specified <paramref name="other"/> has the same width and height as this <see cref="BadgeIcon" />; otherwise, false.
        /// </returns>
        public readonly bool Equals(BadgeIcon other) => this.Title == other.Title && this.Width == other.Width && this.Height == other.Height && this.Content == other.Content;

        /// <summary>
        /// Determines if the two specified <see cref="BadgeIcon" /> objects have equal <see cref="Width"/> and <see cref="Height"/> values.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>
        /// <c>true</c> if the two specified <see cref="BadgeIcon" /> objects have equal <see cref="Width"/> and <see cref="Height"/> values; otherwise, false.
        /// </returns>
        public static bool operator ==(BadgeIcon left, BadgeIcon right) => left.Equals(right);

        /// <summary>
        /// Determines if the two specified <see cref="BadgeIcon" /> objects have different <see cref="Width"/> and/or different <see cref="Height"/> values.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>
        /// <c>true</c> if the two specified <see cref="BadgeIcon" /> objects have different <see cref="Width"/> and/or different <see cref="Height"/> values; otherwise, false.
        /// </returns>
        public static bool operator !=(BadgeIcon left, BadgeIcon right) => !left.Equals(right);

        /// <summary>
        /// Converts the specified <paramref name="content"/> to a new <see cref="BadgeIcon"/> object.
        /// </summary>
        /// <param name="content">The base64 encoded svg image content of the icon.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="content"/> is <c>null</c>.
        /// </exception>
        public static implicit operator BadgeIcon(string content) => new BadgeIcon(content);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri">
        /// The uri of the svg image that will be downloaded and encoded as base64 for the content of the icon.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="uri"/> is <c>null</c>.
        /// </exception>
        public static implicit operator BadgeIcon(Uri uri) => new BadgeIcon(uri);
    }
}
