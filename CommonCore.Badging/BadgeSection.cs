﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;

namespace CommonCore.Badging
{
    /// <summary>
    /// The <see cref="BadgeSection" /> <c>class</c> represents one section of a
    /// <see cref="Badge"/>.
    /// </summary>
    public class BadgeSection
    {
        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance.
        /// </summary>
        public BadgeSection() { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        public BadgeSection(BadgeIcon icon)
            : this(icon, null, BadgeStyle.CreateDefault(), null) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="style"/> for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="style">The style of the icon.</param>
        public BadgeSection(BadgeIcon icon, BadgeStyle style)
            : this(icon, null, style, null) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="backColor"/>
        /// for the specified <paramref name="icon"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="backColor">The background color of the style of the icon.</param>
        public BadgeSection(BadgeIcon icon, Color backColor)
            : this(icon, null, BadgeStyle.CreateDefault(backColor), null) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        public BadgeSection(string text)
            : this(BadgeIcon.None, text, null, BadgeStyle.CreateDefault()) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="style"/> for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the text.</param>
        public BadgeSection(string text, BadgeStyle style)
            : this(BadgeIcon.None, text, null, style) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="backColor"/>
        /// for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the style of the text.</param>
        public BadgeSection(string text, Color backColor)
            : this(BadgeIcon.None, text, null, BadgeStyle.CreateDefault(backColor)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style for the specified <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        public BadgeSection(BadgeIcon icon, string text)
            : this(icon, text, BadgeStyle.CreateDefault(), BadgeStyle.CreateDefault()) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="style"/> for the specified
        /// <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="style">The style of the icon and text.</param>
        public BadgeSection(BadgeIcon icon, string text, BadgeStyle style)
            : this(icon, text, style, style) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="backColor"/>
        /// for the specified <paramref name="icon"/> and <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="backColor">The background color of the style of the icon and text.</param>
        public BadgeSection(BadgeIcon icon, string text, Color backColor)
            : this(icon, text, BadgeStyle.CreateDefault(backColor), BadgeStyle.CreateDefault(backColor)) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the specified <paramref name="iconStyle"/> for the specified <paramref name="icon"/>
        /// and the specified <paramref name="textStyle"/> for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconStyle">The style of the icon.</param>
        /// <param name="textStyle">The style of the text.</param>
        public BadgeSection(BadgeIcon icon, string text, BadgeStyle iconStyle, BadgeStyle textStyle)
        {
            this.Icon = icon.Content;
            this.Text = text;
            this.IconStyle = iconStyle ?? new BadgeStyle();
            this.TextStyle = textStyle ?? new BadgeStyle();
        }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance
        /// using the default style with the specified <paramref name="iconBackColor"/>
        /// for the specified <paramref name="icon"/>
        /// using the default style with the specified <paramref name="iconBackColor"/>
        /// for the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="icon">The icon of the section.</param>
        /// <param name="text">The text of the section.</param>
        /// <param name="iconBackColor">The background color of the style of the icon.</param>
        /// <param name="textBackColor">The background color of the style of the text.</param>
        public BadgeSection(BadgeIcon icon, string text, Color iconBackColor, Color textBackColor)
            : this(icon, text, BadgeStyle.CreateDefault(iconBackColor), BadgeStyle.CreateDefault(textBackColor)) { }

        /// <summary>
        /// Gets or sets the base64 encoded svg icon of the section.
        /// </summary>
        public string Icon { get; set; } = null;

        /// <summary>
        /// Gets or sets the text of the section.
        /// </summary>
        public string Text { get; set; } = null;

        /// <summary>
        /// Gets or sets the style used for the icon of the section.
        /// This property has no effect if <see cref="Icon"/> is <c>null</c> or <c>empty</c>.
        /// </summary>
        public BadgeStyle IconStyle { get; set; } = new BadgeStyle();

        /// <summary>
        /// Gets or sets the style used for the text of the section.
        /// This property has no effect if <see cref="Text"/> is <c>null</c> or <c>empty</c>.
        /// </summary>
        public BadgeStyle TextStyle { get; set; } = new BadgeStyle();

        /// <summary>
        /// Gets a value indicating if the section text is non-empty.
        /// Returns <c>true</c> if <see cref="Text"/> is not <c>null</c> and not <c>empty</c>; otherwise <c>false</c>.
        /// </summary>
        public bool TextExists => !string.IsNullOrEmpty(this.Text);

        /// <summary>
        /// Gets a value indicating if the section icon is non-empty.
        /// Returns <c>true</c> if <see cref="Icon"/> is not <c>null</c> and not <c>empty</c>; otherwise <c>false</c>.
        /// </summary>
        public bool IconExists => !string.IsNullOrEmpty(this.Icon);

        /// <summary>
        /// Returns a value indicating if neither the section text nor icon exist.
        /// Returns <c>true</c> if <see cref="Text"/> and <see cref="Icon"/> are both <c>null</c> or <c>empty</c>; otherwise <c>false</c>.
        /// </summary>
        /// <returns><c>true</c> if <see cref="Text"/> and <see cref="Icon"/> are both <c>null</c> or <c>empty</c>; otherwise <c>false</c>.</returns>
        public bool IsEmpty() => !this.TextExists && !this.IconExists;

        /// <summary>
        /// Returns a value indicating if either the section text or icon exist.
        /// Returns <c>true</c> if <see cref="Text"/> or <see cref="Icon"/> are not <c>null</c> and not <c>empty</c>; otherwise <c>false</c>.
        /// </summary>
        /// <returns><c>true</c> if <see cref="Text"/> or <see cref="Icon"/> are not <c>null</c> and not <c>empty</c>; otherwise <c>false</c>.</returns>
        public bool IsNotEmpty() => this.TextExists || this.IconExists;

        #region Deprecated Members

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance.
        /// The <see cref="Uri"/> of the <paramref name="icon"/>, if not <c>null</c>,
        /// must represent an svg image and it will be downloaded and then converted
        /// to base64 before being assigned to the instance.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="color">The color of the section.</param>
        /// <param name="icon">The uri of a svg image for the icon of the section.</param>
        /// <param name="textPadding">The padding of the section text.</param>
        /// <param name="iconPadding">The padding of the section icon.</param>
        /// <param name="enableColorGloss">Indicate if gloss is applied to the color of the section.</param>
        /// <param name="enableTextShadow">Indicate if shadow is applied to the text of the section.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public BadgeSection(string text, Color color, Uri icon, float textPadding = 4, float iconPadding = 3, bool enableColorGloss = true, bool enableTextShadow = true)
            : this(text, color, icon is null ? null : BadgeUtils.DownloadBase64(icon), textPadding, iconPadding, enableColorGloss, enableTextShadow) { }

        /// <summary>
        /// Initializes a <see cref="BadgeSection" /> instance.
        /// </summary>
        /// <param name="text">The text of the section.</param>
        /// <param name="color">The color of the section.</param>
        /// <param name="icon">The base64 encoded svg icon of the section.</param>
        /// <param name="textPadding">The padding of the section text.</param>
        /// <param name="iconPadding">The padding of the section icon.</param>
        /// <param name="enableColorGloss">Indicate if gloss is applied to the color of the section.</param>
        /// <param name="enableTextShadow">Indicate if shadow is applied to the text of the section.</param>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use ? instead.", false)]
        public BadgeSection(string text, Color color, string icon = null, float textPadding = 4, float iconPadding = 3, bool enableColorGloss = true, bool enableTextShadow = true)
        {
            this.Text = text;
            this.Color = color;
            this.Icon = icon;
            this.TextPadding = textPadding;
            this.IconPadding = iconPadding;
            this.EnableColorGloss = enableColorGloss;
            this.EnableTextShadow = enableTextShadow;
        }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="TextStyle"/> <see cref="BadgeStyle.Padding"/> instead.
        /// Gets or sets the padding for the text of the section via the <see cref="TextPadding"/> property.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use TextStyle.Padding instead.", false)]
        public float Padding { get => this.TextStyle.Padding; set => this.TextStyle.Padding = (int)value; }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="IconStyle"/> and/or <see cref="TextStyle"/> <see cref="BadgeStyle.BackColor"/> instead.
        /// Gets or sets the color of the section.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use IconStyle.BackColor and/or TextStyle.BackColor instead.", false)]
        public Color Color { get; set; } = BadgeColors.Default;

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="TextStyle"/> <see cref="BadgeStyle.Font"/> instead.
        /// Gets or sets the font for the text of the section.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use TextStyle.Font.", false)]
        public BadgeFont Font { get; set; } = new BadgeFont();

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="TextStyle"/> <see cref="BadgeStyle.Padding"/> instead.
        /// Gets or sets the padding for the text of the section.
        /// The padding is applied to both the left and right sides of the text.
        /// This property has no effect if <see cref="Text"/> is <c>empty</c>.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use TextStyle.Padding instead.", false)]
        public float TextPadding { get => this.TextStyle.Padding; set => this.TextStyle.Padding = (int)value; }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="IconStyle"/> <see cref="BadgeStyle.Padding"/> instead.
        /// Gets or sets the padding for the icon of the section.
        /// The padding is applied to all four sides of the icon.
        /// This property has no effect if <see cref="Icon"/> is <c>empty</c>.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use IconStyle.Padding instead.", false)]
        public float IconPadding { get => this.IconStyle.Padding; set => this.IconStyle.Padding = (int)value; }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="IconStyle"/> and/or <see cref="TextStyle"/> <see cref="BadgeStyle.EnableGloss"/> instead.
        /// Gets or sets whether a linear gradient gloss effect is applied to the section color.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use IconStyle.EnableGloss and/or TextStyle.EnableGloss instead.", false)]
        public bool EnableColorGloss { get => this.TextStyle.EnableGloss || this.IconStyle.EnableGloss; set => this.TextStyle.EnableGloss = this.IconStyle.EnableGloss = value; }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="TextStyle"/> <see cref="BadgeStyle.EnableShadow"/> instead.
        /// Gets or sets whether a shadow effect is applied to the section text.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use TextStyle.EnableShadow instead.", false)]
        public bool EnableTextShadow { get => this.TextStyle.EnableShadow; set => this.TextStyle.EnableShadow = value; }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder.Default"/>
        /// <see cref="BadgeBuilder.CalIconPaddedWidth(Badge, BadgeSection)"/> instead.
        /// Calculate the width of the section icon.
        /// </summary>
        /// <returns>The width of the section icon.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder.Default.CalIconPaddedWidth() instead.", false)]
        public int CalIconWidth() => this.IconExists ? 20 : 0;

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder.Default"/>
        /// <see cref="BadgeBuilder.CalTextPaddedWidth(Badge, BadgeSection)"/> instead.
        /// Calculate the width of the section text.
        /// </summary>
        /// <returns>The width of the section text.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder.Default.CalTextPaddedWidth() instead.", false)]
        public int CalTextWidth()
        {
            if (this.TextExists)
            {
                float width = this.TextStyle.Font.CalTextWidth(this.Text);
                if (width > (int)width) { width++; }
                width += this.TextStyle.Padding * 2;
                return (int)width;
            }
            return 0;
        }

        /// <summary>
        /// !!DEPRECATED!! Use <see cref="BadgeBuilder.Default"/>
        /// <see cref="BadgeBuilder.CalSectionWidth(Badge, BadgeSection)"/> instead.
        /// Calculate the width of the section.
        /// </summary>
        /// <returns>The width of the section.</returns>
        [ExcludeFromCodeCoverage]
        [Obsolete("!!DEPRECATED!! Use BadgeBuilder.Default.CalSectionWidth() instead.", false)]
        public int CalWidth() => this.CalIconWidth() + this.CalTextWidth();

        #endregion
    }
}
