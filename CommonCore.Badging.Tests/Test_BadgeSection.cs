﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeSection
    {
        [Fact]
        public void Constructor_Default()
        {
            var section = new BadgeSection();
            section.Text.Should().BeNull();
            section.Icon.Should().BeNull();

            section.TextExists.Should().BeFalse();
            section.IconExists.Should().BeFalse();

            section.IsEmpty().Should().BeTrue();
            section.IsNotEmpty().Should().BeFalse();

            section.IconStyle.Should().NotBeNull();
            section.IconStyle = null;
            section.IconStyle.Should().BeNull();
            section.IconStyle = new BadgeStyle();
            section.IconStyle.Should().NotBeNull();

            section.TextStyle.Should().NotBeNull();
            section.TextStyle = null;
            section.TextStyle.Should().BeNull();
            section.TextStyle = new BadgeStyle();
            section.TextStyle.Should().NotBeNull();
        }
    }
}
