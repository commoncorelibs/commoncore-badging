﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeStyle
    {
        [Fact]
        public void CreateDefault()
        {
            BadgeStyle.CreateDefault().Should().NotBeNull();

            var style = BadgeStyle.CreateDefault();
            style.BackColor.Should().Be(BadgeColors.Default);
            style.ForeColor.Should().Be(BadgeColors.White);
            style.Padding.Should().Be(4);
            style.EnableGloss.Should().BeTrue();
            style.EnableShadow.Should().BeTrue();

            style.FontSize.Should().Be(11);

            var color = BadgeColors.DarkBlue;
            BadgeStyle.CreateDefault(color).Should().NotBeNull();

            style = BadgeStyle.CreateDefault(color);
            style.BackColor.Should().Be(color);
            style.ForeColor.Should().Be(BadgeColors.White);
            style.Padding.Should().Be(4);
            style.EnableGloss.Should().BeTrue();
            style.EnableShadow.Should().BeTrue();

            style.FontSize.Should().Be(11);
            style.FontSize = 8;
            style.FontSize.Should().Be(8);
            style.FontSize = 11;
            style.FontSize.Should().Be(11);

            style.Font.Should().NotBeNull();
            style.Font = null;
            style.Font.Should().BeNull();
            style.Font = new BadgeFont();
            style.Font.Should().NotBeNull();
        }

        [Fact]
        public void Constructor_Default()
        {
            var style = new BadgeStyle();
            style.BackColor.Should().Be(BadgeColors.Default);
            style.ForeColor.Should().Be(BadgeColors.White);
            style.Padding.Should().Be(0);
            style.EnableGloss.Should().BeFalse();
            style.EnableShadow.Should().BeFalse();

            style.FontSize.Should().Be(11);

            style.Font.Should().NotBeNull();
        }

        [Fact]
        public void Constructor_Standard()
        {
            var backColor = BadgeColors.DarkViolet;
            var foreColor = BadgeColors.LightSalmon;
            int padding = 10;
            bool enableGloss = true;
            bool enableShadow = false;
            var style = new BadgeStyle(backColor, foreColor, padding, enableGloss, enableShadow);
            style.BackColor.Should().Be(backColor);
            style.ForeColor.Should().Be(foreColor);
            style.Padding.Should().Be(padding);
            style.EnableGloss.Should().Be(enableGloss);
            style.EnableShadow.Should().Be(enableShadow);

            style.FontSize.Should().Be(11);

            style.Font.Should().NotBeNull();
        }
    }
}
