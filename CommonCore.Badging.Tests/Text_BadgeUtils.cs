﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Text_BadgeUtils
    {
        [Theory]
        [InlineData(KnownColor.Black, "#000000")]
        [InlineData(KnownColor.White, "#FFFFFF")]
        [InlineData(KnownColor.Red, "#FF0000")]
        [InlineData(KnownColor.Lime, "#00FF00")]
        [InlineData(KnownColor.Blue, "#0000FF")]
        public void ToHex(KnownColor subject, string expected)
            => BadgeUtils.ToHex(Color.FromKnownColor(subject)).Should().Be(expected);

        [Theory]
        [InlineData(KnownColor.Black, "0,0,0")]
        [InlineData(KnownColor.White, "255,255,255")]
        [InlineData(KnownColor.Red, "255,0,0")]
        [InlineData(KnownColor.Lime, "0,255,0")]
        [InlineData(KnownColor.Blue, "0,0,255")]
        public void ToRgb(KnownColor subject, string expected)
            => BadgeUtils.ToRgb(Color.FromKnownColor(subject)).Should().Be(expected);

        [Theory]
        [InlineData("", "")]
        [InlineData("some text!", "c29tZSB0ZXh0IQ==")]
        public void ToBase64(string subject, string expected)
            => BadgeUtils.ToBase64(subject).Should().Be(expected);

        [Fact]
        public void ThrowArgNull()
        {
            Action act;

            act = () => BadgeUtils.ThrowArgNull((object)null, "test");
            act.Should().Throw<ArgumentNullException>();

            act = () => BadgeUtils.ThrowArgNull(new object(), "test");
            act.Should().NotThrow();
        }
    }
}
