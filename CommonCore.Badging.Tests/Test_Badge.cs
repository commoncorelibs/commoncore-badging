﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_Badge
    {
        [Fact]
        public void Basics()
        {
            var badge = new Badge();
            badge.Height.Should().Be(20);

            badge.Roundness.Should().Be(3);
            badge.Roundness = 5;
            badge.Roundness.Should().Be(5);
            badge.Roundness = 3;
            badge.Roundness.Should().Be(3);
        }

        [Fact]
        public void Constructor_Default()
        {
            var badge = new Badge();
            badge.Sections.Should().BeEmpty();

            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.ToString().Should().NotBeNullOrEmpty();
            badge.ToString(false).Should().NotBeNullOrEmpty();
            badge.ToString(EBadgeFormat.Svg).Should().NotBeNullOrEmpty();
            badge.ToString(EBadgeFormat.Base64).Should().NotBeNullOrEmpty();

            badge.ToSvg().Should().NotBeNullOrEmpty();
            badge.ToSvg(false).Should().NotBeNullOrEmpty();

            badge.ToBase64().Should().NotBeNullOrEmpty();
            badge.ToBase64(false).Should().NotBeNullOrEmpty();
        }

        [Fact]
        public void Constructor_Enumerable()
        {
            Action act = () => new Badge((List<BadgeSection>)null);
            act.Should().Throw<ArgumentNullException>();

            var badge = new Badge(new List<BadgeSection>() { new BadgeSection(BadgeIcons.Info, "some text") });
            badge.Sections.Should().HaveCount(1);

            badge.IsEmpty().Should().BeFalse();
            badge.IsNotEmpty().Should().BeTrue();

            badge.ToString().Should().NotBeNullOrEmpty();
            badge.ToString(false).Should().NotBeNullOrEmpty();
            badge.ToString(EBadgeFormat.Svg).Should().NotBeNullOrEmpty();
            badge.ToString(EBadgeFormat.Base64).Should().NotBeNullOrEmpty();

            badge.ToSvg().Should().NotBeNullOrEmpty();
            badge.ToSvg(false).Should().NotBeNullOrEmpty();

            badge.ToBase64().Should().NotBeNullOrEmpty();
            badge.ToBase64(false).Should().NotBeNullOrEmpty();
        }

        [Fact]
        public void IsEmpty()
        {
            Badge badge = new Badge();
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.Sections.Add(new BadgeSection());
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.Sections[0].Text = "some text";
            badge.IsEmpty().Should().BeFalse();
            badge.IsNotEmpty().Should().BeTrue();

            badge.Sections[0].Text = string.Empty;
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.Sections[0].Text = null;
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.Sections[0].Icon = BadgeIcons.Info;
            badge.IsEmpty().Should().BeFalse();
            badge.IsNotEmpty().Should().BeTrue();

            badge.Sections[0].Icon = string.Empty;
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();

            badge.Sections[0].Icon = null;
            badge.IsEmpty().Should().BeTrue();
            badge.IsNotEmpty().Should().BeFalse();
        }

        [Fact]
        [Obsolete("BadgeSection.Cal*() methods are deprecated. Remove this test when they are removed.", false)]
        public void Obsolete_CalMethods()
        {
            var badge = new Badge();
            badge.CalWidth().Should().Be(0);
            
            badge = new Badge(new List<BadgeSection>() { new BadgeSection("some text", BadgeColors.Default, BadgeIcons.Info) });
            badge.CalWidth().Should().BeGreaterThan(0);
        }
    }
}
