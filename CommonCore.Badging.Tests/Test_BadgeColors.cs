﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeColors
    {
        [Fact]
        public void Properties()
        {
            var props = typeof(BadgeColors).GetProperties(BindingFlags.Static | BindingFlags.Public)
                .Where(prop => prop.PropertyType == typeof(Color))
                .ToArray();
            foreach (var prop in props) { ((Color)prop.GetValue(null)).Should().NotBeNull(); }
        }

        [Fact]
        public void Dict()
        {
            BadgeColors.Dict.Should().NotBeNull().And.NotBeEmpty();
            foreach (var pair in BadgeColors.Dict) { pair.Value.Should().NotBeNull(); }
        }
    }
}
