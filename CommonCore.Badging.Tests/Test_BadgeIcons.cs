﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using Microsoft.VisualStudio.TestPlatform.Common.ExtensionFramework.Utilities;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeIcons
    {
        [Fact]
        public void Properties()
        {
            var props = typeof(BadgeIcons).GetProperties(BindingFlags.Static | BindingFlags.Public)
                .Where(prop => prop.PropertyType == typeof(string))
                .ToArray();
            foreach (var prop in props)
            {
                var value = (string)prop.GetValue(null);
                value.Should().NotBeNull();
                ValidateBase64(value).Should().BeTrue();
            }
        }

        [Fact]
        public void Dict()
        {
            BadgeIcons.Dict.Should().NotBeNull().And.NotBeEmpty();
            foreach (var pair in BadgeIcons.Dict)
            {
                pair.Value.Should().NotBeNull();
                ValidateBase64(pair.Value).Should().BeTrue();
            }
        }

        private static bool ValidateBase64(ReadOnlySpan<char> text)
        {
            // Empty is valid to accommodate no image.
            if (text.IsEmpty) { return true; }
            // Base64 string length must be multiple of 4.
            else if (text.Length % 4 != 0) { return false; }
            else
            {
                //Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,2}$")
                int padding = text[^1] == '=' && text[^2] == '=' ? 2 : text[^1] == '=' ? 1 : 0;
                foreach (var c in text[0..^padding])
                {
                    if (c == '+' || c == '/' || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) { continue; }
                    else { return false; }
                }
                return true;
            }
        }
    }
}
