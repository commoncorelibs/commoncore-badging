﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeFonts
    {
        [Fact]
        public void Properties()
        {
            var props = typeof(BadgeFonts).GetProperties(BindingFlags.Static | BindingFlags.Public)
                .Where(prop => prop.PropertyType == typeof(BadgeFont))
                .ToArray();
            foreach (var prop in props) { ((BadgeFont)prop.GetValue(null)).Should().NotBeNull(); }
        }

        [Fact]
        public void Dict()
        {
            BadgeFonts.Dict.Should().NotBeNull().And.NotBeEmpty();
            foreach (var pair in BadgeFonts.Dict) { pair.Value.Should().NotBeNull(); }
        }
    }
}
