﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeBuilder
    {
        [Fact]
        public void Default()
            => BadgeBuilder.Default.Should().NotBeNull();

        [Fact]
        public void AppendRoundedRect()
        {
            StringBuilder builder;

            BadgeBuilder.Default.AppendRoundedRect(builder = new StringBuilder(), Point.Empty, new Size(40, 20), 5, true, true, BadgeUtils.ToHex(Color.DarkBlue));
            builder.ToString().Should().NotBeEmpty();

            BadgeBuilder.Default.AppendRoundedRect(builder = new StringBuilder(), Point.Empty, new Size(40, 20), 5, true, false, BadgeUtils.ToHex(Color.DarkBlue));
            builder.ToString().Should().NotBeEmpty();

            BadgeBuilder.Default.AppendRoundedRect(builder = new StringBuilder(), Point.Empty, new Size(40, 20), 5, false, true, BadgeUtils.ToHex(Color.DarkBlue));
            builder.ToString().Should().NotBeEmpty();

            BadgeBuilder.Default.AppendRoundedRect(builder = new StringBuilder(), Point.Empty, new Size(40, 20), 5, false, false, BadgeUtils.ToHex(Color.DarkBlue));
            builder.ToString().Should().NotBeEmpty();
        }

        [Fact]
        public void Cal_Methods()
        {
            BadgeSection section;
            Badge badge = new Badge();

            BadgeBuilder.Default.CalBadgeWidth(badge).Should().Be(0);
            BadgeBuilder.Default.CalBadgeSize(badge).Should().Be(Size.Empty);

            badge.Sections.Add(new BadgeSection());

            BadgeBuilder.Default.CalBadgeWidth(badge).Should().Be(0);
            BadgeBuilder.Default.CalBadgeSize(badge).Should().Be(Size.Empty);

            section = badge.Sections[0];
            BadgeBuilder.Default.CalSectionWidth(badge, section).Should().Be(0);
            BadgeBuilder.Default.CalIconPaddedWidth(badge, section).Should().Be(0);
            BadgeBuilder.Default.CalTextPaddedWidth(badge, section).Should().Be(0);

            BadgeBuilder.Default.CalSectionSize(badge, section).Should().Be(Size.Empty);
            BadgeBuilder.Default.CalIconSize(badge, section).Should().Be(Size.Empty);
            BadgeBuilder.Default.CalTextSize(badge, section).Should().Be(Size.Empty);

            BadgeBuilder.Default.CalIconRect(badge, section).Should().Be(Rectangle.Empty);
            BadgeBuilder.Default.CalTextRect(badge, section).Should().Be(Rectangle.Empty);

            badge.Sections.Add(new BadgeSection(BadgeIcons.Info, "some text"));

            BadgeBuilder.Default.CalBadgeWidth(badge).Should().BeGreaterThan(0);
            BadgeBuilder.Default.CalBadgeSize(badge).Should().NotBe(Size.Empty);

            section = badge.Sections[1];
            BadgeBuilder.Default.CalSectionWidth(badge, section).Should().BeGreaterThan(0);
            BadgeBuilder.Default.CalIconPaddedWidth(badge, section).Should().BeGreaterThan(0);
            BadgeBuilder.Default.CalTextPaddedWidth(badge, section).Should().BeGreaterThan(0);

            BadgeBuilder.Default.CalSectionSize(badge, section).Should().NotBe(Size.Empty);
            BadgeBuilder.Default.CalIconSize(badge, section).Should().NotBe(Size.Empty);
            BadgeBuilder.Default.CalTextSize(badge, section).Should().NotBe(Size.Empty);

            BadgeBuilder.Default.CalIconRect(badge, section).Should().NotBe(Rectangle.Empty);
            BadgeBuilder.Default.CalTextRect(badge, section).Should().NotBe(Rectangle.Empty);
        }
    }
}
