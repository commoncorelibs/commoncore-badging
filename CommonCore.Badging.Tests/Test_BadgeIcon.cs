﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;
using System.Drawing;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeIcon
    {
        [Fact]
        public void Equality()
        {
            BadgeIcon.None.Equals(new object()).Should().BeFalse();
            BadgeIcon.None.Equals((object)new BadgeIcon()).Should().BeTrue();

            BadgeIcon.None.Equals(new BadgeIcon()).Should().BeTrue();
            BadgeIcon.None.Equals(new BadgeIcon(BadgeIcons.Info)).Should().BeFalse();

            (BadgeIcon.None == new BadgeIcon()).Should().BeTrue();
            (BadgeIcon.None == new BadgeIcon(BadgeIcons.Info)).Should().BeFalse();

            (BadgeIcon.None != new BadgeIcon()).Should().BeFalse();
            (BadgeIcon.None != new BadgeIcon(BadgeIcons.Info)).Should().BeTrue();

            BadgeIcon.None.GetHashCode().Should().Be(new BadgeIcon().GetHashCode());
            BadgeIcon.None.GetHashCode().Should().NotBe(new BadgeIcon(BadgeIcons.Info).GetHashCode());

            new BadgeIcon(BadgeIcons.Info).Equals(BadgeIcons.Info).Should().BeTrue();
        }

        [Fact]
        public void None()
        {
            BadgeIcon.None.Should().NotBeNull();

            BadgeIcon.None.Title.Should().BeNull();
            BadgeIcon.None.Width.Should().Be(0);
            BadgeIcon.None.Height.Should().Be(0);
            BadgeIcon.None.Content.Should().BeNull();

            BadgeIcon.None.ContentExists.Should().BeFalse();
            BadgeIcon.None.Size.Should().Be(Size.Empty);
        }

        [Fact]
        public void Constructor_Meta_String()
        {
            string title = "test";
            int width = 24;
            int height = 24;
            string content = BadgeIcons.Info;
            var icon = new BadgeIcon(title, width, height, content);

            icon.Title.Should().Be(title);
            icon.Width.Should().Be(width);
            icon.Height.Should().Be(height);
            icon.Content.Should().Be(content);

            icon.ContentExists.Should().BeTrue();
            icon.Size.Should().Be(new Size(width, height));
        }

        [Fact]
        public void Constructor_String()
        {
            string content = BadgeIcons.Info;
            var icon = new BadgeIcon(content);

            icon.Title.Should().BeNull();
            icon.Width.Should().Be(20);
            icon.Height.Should().Be(20);
            icon.Content.Should().Be(content);

            icon.ContentExists.Should().BeTrue();
            icon.Size.Should().Be(new Size(20, 20));
        }
    }
}
