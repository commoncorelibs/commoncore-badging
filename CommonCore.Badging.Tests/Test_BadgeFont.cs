﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Badging.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_BadgeFont
    {
        [Fact]
        public void Basics()
        {
            Action act;
            BadgeFont font;

            act = () => new BadgeFont();
            act.Should().NotThrow();

            font = new BadgeFont();
            font.Name.Should().Be(BadgeFont.DefaultFontName);
            font.Fixed.Should().Be(BadgeFont.DefaultFontFixed);

            act = () => new BadgeFont(null, BadgeFont.DefaultFontFixed);
            act.Should().Throw<ArgumentNullException>();

            font = new BadgeFont(BadgeFont.DefaultFontName, BadgeFont.DefaultFontFixed);
            font.Name.Should().Be(BadgeFont.DefaultFontName);
            font.Fixed.Should().Be(BadgeFont.DefaultFontFixed);

            var text = "test text";
            SizeF size = font.CalTextSize(text);
            ((int)size.Width).Should().BeCloseTo(47, 1);
            ((int)size.Height).Should().BeCloseTo(13, 1);
            float width = font.CalTextWidth(text);
            ((int)width).Should().BeCloseTo(47, 1);

            font.Name = null;
            size = font.CalTextSize(text);
            ((int)size.Width).Should().BeCloseTo(47, 1);
            ((int)size.Height).Should().BeCloseTo(13, 1);
            width = font.CalTextWidth(text);
            ((int)width).Should().BeCloseTo(47, 1);
        }
    }
}
