# Contributing to CommonCore.Badging

All contributions are welcome. If you have questions, feel free to open an issue.

## Adding to BadgeIcons

Before a svg image can be added as a `BadgeIcons` icon, the following steps must be taken:

- If needed, the image should be [resized](https://www.iloveimg.com/resize-image/resize-svg) to 24x24 with `viewBox="0 0 24 24"` added to the svg element.
- The image must be [optimized](https://jakearchibald.github.io/svgomg/) to avoid wasted space.
- Convert the svg image to a base64 encoded string.
