## 1.2.0

Major overhauls in this release.

* The seven common repo badge colors have been included.
* Near complete rewrite of the svg output generation.
* Near complete replacement of Badge and BadgeSection constructors.
* Icon and text areas can have different background colors.
* Icon and text areas can independently apply gloss effect.
* Icon area can have a shadow effect independent of the text shadow state.
* Icon content can have a foreground color applied to it.

### Deprecated

* BadgeFont(string, int, Color, bool) #35
* BadgeFont.Color #35
* BadgeFont.DefaultFontColor #35
* BadgeFont.Size #41
* BadgeFont.DefaultFontSize #41

* Badge(string, Color, string) #35
* Badge(string, string, string, Color) #35
* Badge.CalWidth() #32

* BadgeSection(string, Color, Uri, float, float, bool, bool) #35
* BadgeSection(string, Color, string, float, float, bool, bool) #35
* BadgeSection.Color #37
* BadgeSection.EnableColorGloss #35
* BadgeSection.EnableTextShadow #35
* BadgeSection.TextPadding #35
* BadgeSection.IconPadding #35
* Badge.CalWidth() #32
* Badge.CalIconWidth() #32
* Badge.CalTextWidth() #32

* Badging_MiscExtensions class #40
* Badging_MiscExtensions.ToHex() #40
* Badging_MiscExtensions.ToRgb() #40
* Badging_MiscExtensions.ToBase64() #40
* Badging_MiscExtensions.DownloadString() #40
* Badging_MiscExtensions.DownloadBase64() #40
* Badging_MiscExtensions.ThrowArgNull() #40

### Fixed

* Empty sections cause gloss gradient def inclusion #29
* Svg gradient element should be nested in defs element #30
* Some svg renderers ignore rounded corners #31
* BadgeBuilder should html encode text content #42

### Added

* BadgeIcon record #22
* BadgeBuilder class #32
* BadgeStyle class #34
* BadgeFonts static class #17

* Badge.Height #27
* Badge.Roundness #28
* Badge.Count #44
* Badge[] operator #44
* Badge : IEnumerable #44

* BadgeColors.BGreen as HEX (RGB): #33CC11 (51, 204, 17) #16
* BadgeColors.BBlue as HEX (RGB): #0088CC (0, 136, 204) #16
* BadgeColors.BYellow as HEX (RGB): #DDBB11 (221, 187, 17) #16
* BadgeColors.BOrange as HEX (RGB): #FF7733 (255, 119, 51) #16
* BadgeColors.BRed as HEX (RGB): #EE4433 (238, 68, 51) #16
* BadgeColors.BLightGray as HEX (RGB): #999999 (153, 153, 153) #16
* BadgeColors.BDarkGray as HEX (RGB): #555555 (85, 85, 85) #16

### Misc

* CICD scripting rewritten
* Version now stored in repo text file

## 1.1.2

Patch release to fix a bug.

### Fixed

* Section icons not left aligned #25

## 1.1.1

Patch release to fix a bug.

### Fixed

* BadgeFont.CalTextSize() throws when called concurrently #24

### Misc

* Added basic unit tests #18

## 1.1.0

Fixed several bugs, exposed existing functionality to user control, commented most of the code, and generally improved the code. Highlights include better formed svg, more accurate vertical text alignment, and applying of the color gloss and text shadow effects can now be disabled.

### Deprecated

* BadgeSection.Padding #8

### Fixed

* BadgeSection.TextPadding should initialize to 0 #1, #8
* Blank svg text sections should be skipped #4
* BadgeFont.CalTextSize hardcodes measurement font #10
* BadgeSection.Color should initialize to BadgeColors.Default #14

### Added

* Badge.ToSvg methods #2
* BadgeSection default constructor #15
* BadgeSection.EnableColorGloss property #11
* BadgeSection.EnableTextShadow property #13
* BadgeSection.IconPadding property #12
* BadgeSection.TextPadding property #8
* BadgeSection.CalIconWidth() method #6
* BadgeSection.CalTextWidth() method #6
* BadgeSection.IsEmpty() methods #5
* BadgeFont.DefaultFontFixed static property #9
* BadgeFont.CalTextSize() method #7

### Misc

* Added basic documentation comments to nearly all the code. #3

## 1.0.0

* Initial project creation.
